/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework19;

/**
 *
 * @author slavi
 */
 class Task {

    private String name;
    private int workingHours;

    Task(String name, int workingHours) {
        this.setName(name);
        this.setWorkingHours(workingHours);
    }

    String getName() {
        return name;
    }

    int getWorkingHours() {
        return workingHours;
    }

    final void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Incorrect name!");
            return;
        }
        this.name = name;
    }

    final void setWorkingHours(int workingHours) {
        if (workingHours < 0 || workingHours > 500) {
            System.err.println("Incorrect workingHours");
            return;
        }
        this.workingHours = workingHours;
    }
}
