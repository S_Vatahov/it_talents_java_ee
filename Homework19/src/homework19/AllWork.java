/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework19;

/**
 *
 * @author slavi
 */
public class AllWork {

    private final Task[] tasks;
    private int freePlacesForTasks;
    private int currentUnassignedTask;

    static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    static final String ANSI_RED_BACKGROUND = "\u001B[41m";

    AllWork() {
        this.setFreePlacesForTasks(10);
        this.tasks = new Task[this.getFreePlacesForTasks()];
        this.currentUnassignedTask = -1;
    }

    final int getFreePlacesForTasks() {
        return freePlacesForTasks;
    }

    final void setFreePlacesForTasks(int freePlacesForTasks) {
        if (freePlacesForTasks < 0 || freePlacesForTasks > 10) {
            return;
        }
        this.freePlacesForTasks = freePlacesForTasks;
    }

    final int getCurrentUnassignedTask() {
        return currentUnassignedTask;
    }

    final void setCurrentUnassignedTask(int currentUnassignedTask) {
        if (currentUnassignedTask < -1 || currentUnassignedTask > 9) {
            return;
        }
        this.currentUnassignedTask = currentUnassignedTask;
    }

    final Task[] getTasks() {
        return tasks;
    }

    void addTask(Task newTask) {
        if (newTask == null) {
            return;
        }
        if (getFreePlacesForTasks() == 0) {
            System.err.println("There are no free seats");
            return;
        }
        this.tasks[this.tasks.length - this.getFreePlacesForTasks()] = newTask;
        this.setFreePlacesForTasks(this.getFreePlacesForTasks() - 1);
        this.setCurrentUnassignedTask(this.getCurrentUnassignedTask() + 1);
    }

    Task getNextTask() {
        if (getCurrentUnassignedTask() < 0) {
            System.out.println("There is no further task!");
            return null;
        }
        Task tempTask = this.tasks[getCurrentUnassignedTask()];
        this.tasks[getCurrentUnassignedTask()] = null;
        this.setCurrentUnassignedTask(this.getCurrentUnassignedTask() - 1);
        this.setFreePlacesForTasks(this.getFreePlacesForTasks() + 1);
        return tempTask;
    }

    boolean isAllWorkDone() {
        for (Task task : this.tasks) {
            if (task.getWorkingHours() > 0) {
                return false;
            }
        }
        return true;
    }
}
