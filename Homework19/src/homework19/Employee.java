/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework19;

/**
 *
 * @author slavi
 */
class Employee {

    private String name;
    private Task currentTask;
    private int hoursLeft;
    static AllWork allWork;
    static int workDay;

    Employee(String name, AllWork allwork) {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            System.err.println("Incorrect name");
        }
        Employee.allWork = allwork;
    }

    void work() {
        if (currentTask == null) {
            this.setCurrentTask(Employee.allWork.getNextTask());
            if (currentTask == null) {
                return;
            }
        }
        if (currentTask.getWorkingHours() > this.getHoursLeft()) {
            currentTask.setWorkingHours(currentTask.getWorkingHours() - this.getHoursLeft());
            this.setHoursLeft(0);
            System.out.println(this);

        } else {
            this.setHoursLeft(this.getHoursLeft() - currentTask.getWorkingHours());
            this.getCurrentTask().setWorkingHours(0);
            System.out.println(this);
            this.currentTask = null;
            if (this.getHoursLeft() != 0 && Employee.allWork.getCurrentUnassignedTask() >= 0) {
                work();
            }

        }
    }

    @Override
    public String toString() {
        if (this.getCurrentTask() == null) {
            return null;
        }
        String leftAlginFormat = "| %-8s | %-17s | %-10d | %-15d |";
        return String.format(AllWork.ANSI_GREEN_BACKGROUND + leftAlginFormat, this.name, this.currentTask.getName(), this.hoursLeft, this.currentTask.getWorkingHours());
    }

    Task getCurrentTask() {
        return currentTask;

    }

    int getHoursLeft() {
        return hoursLeft;
    }

    void setCurrentTask(Task currentTask) {
        if (currentTask == null) {
            System.err.println(this.name + " Incorrect task");
            return;
        }
        this.currentTask = currentTask;
    }

    private void setHoursLeft(int hoursLeft) {
        if (hoursLeft > 8 || hoursLeft < 0) {
            System.err.println("Incorrect hours left");
            return;
        }
        this.hoursLeft = hoursLeft;
    }

    void startWorkingDay() {
        this.setHoursLeft(8);
        this.work();
    }

     String getName() {
        return name;
    }
}
