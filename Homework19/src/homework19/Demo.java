/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework19;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Demo {

    private static ArrayList<Employee> employers;

    private static void createWorker(Employee employee) {
        employers.add(employee);
    }

    private static void startWorking(AllWork allWork) {
        while (allWork.getCurrentUnassignedTask() > 0) {
            Employee.workDay++;
            System.out.format(AllWork.ANSI_RED_BACKGROUND + "|                             %-30s  |%n", "Day" + Employee.workDay);
            System.out.format(AllWork.ANSI_CYAN_BACKGROUND + "|   Name   |     Task name     | Hours left | Task hours left |%n");
            System.out.format(AllWork.ANSI_CYAN_BACKGROUND + "+----------+-------------------+------------+-----------------+%n");
            for (int i = 0; i < employers.size(); i++) {
                employers.get(i).startWorkingDay();
            }
            System.out.format("+----------+-------------------+------------+-----------------+%n");
        }

        while (true) {
            boolean isWork = false;
            Employee.workDay++;
            System.out.format(AllWork.ANSI_RED_BACKGROUND + "|                             %-30s  |%n", "Day" + Employee.workDay);
            System.out.format(AllWork.ANSI_CYAN_BACKGROUND + "|   Name   |     Task name     | Hours left | Task hours left |%n");
            System.out.format(AllWork.ANSI_CYAN_BACKGROUND + "+----------+-------------------+------------+-----------------+%n");
            for (int i = 0; i < employers.size(); i++) {
                if (employers.get(i).getCurrentTask() != null && employers.get(i).getCurrentTask().getWorkingHours() > 0) {
                    employers.get(i).startWorkingDay();
                    isWork = true;
                }
            }
            System.out.format("+----------+-------------------+------------+-----------------+%n");
            if (!isWork) {
                return;
            }
        }
    }

    public static void main(String[] args) {
        employers = new ArrayList<>();
        AllWork allwork = new AllWork();
        allwork.addTask(new Task("Teaches", 4));
        allwork.addTask(new Task("Drink medication", 500));
        allwork.addTask(new Task("Fix cars", 4));
        allwork.addTask(new Task("Cook", 8));
        allwork.addTask(new Task("Fly by plane", 11));
        allwork.addTask(new Task("Clean", 3));
        allwork.addTask(new Task("Bus driving", 6));
        allwork.addTask(new Task("Taxi driving", 7));
        allwork.addTask(new Task("Training", 6));
        allwork.addTask(new Task("Program", 5));

        createWorker(new Employee("Pesho", allwork));
        createWorker(new Employee("Ginka", allwork));
        createWorker(new Employee("Ali Raza", allwork));

        startWorking(allwork);

    }
}
