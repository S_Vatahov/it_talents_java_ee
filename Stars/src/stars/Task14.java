/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task14 {

    public static void main(String[] args) {
        int n = 10;
        int inSpace = n - 3;
        for (int i = 0; i < n; i++) {
            int space = i;
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            if (i == 0 || i == n - 2 || i == n - 1) {
                for (int j = 0; j < n - i; j++) {
                    System.out.print("*");
                }
            } else {
                int curSpace = inSpace;
                System.out.print("*");
                while (curSpace > 0) {
                    System.out.print(" ");
                    curSpace--;
                }
                System.out.print("*");
                inSpace--;
            }
            System.out.println();
        }
    }
}
