/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task25 {

    public static void main(String[] args) {
        int n = 5;
        for (int i = 0; i < (n * 2) - 1; i++) {
            int space = n - 1;
            if (i == n - 1) {
                for (int j = 0; j < (n * 2) - 1; j++) {
                    System.out.print("+");
                }
                System.out.println();
                continue;
            }
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            System.out.println("+");
        }
    }
}
