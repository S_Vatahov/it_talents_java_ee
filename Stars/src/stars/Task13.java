/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task13 {

    public static void main(String[] args) {
        int n = 5;
        for (int i = 0; i < n; i++) {
            int space = i;
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            for (int j = 0; j < n - i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
