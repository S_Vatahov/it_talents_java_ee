/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task28 {

    public static void main(String[] args) {
        int n = 5;
        int stars = ((n - 2) * 2) - 1;

        for (int i = 2; i < n; i++) {
            int space = n - i - 1;
            while (space > 0) {
                System.err.print(" ");
                space--;
            }
            for (int j = 0; j < stars; j++) {
                System.err.print("*");
            }
            int inSpace = (n * 2) - stars;
            while (inSpace > 0) {
                System.err.print(" ");
                inSpace--;
            }
            for (int j = 0; j < stars; j++) {
                System.err.print("*");
            }
            stars += 2;
            System.err.println();
        }
        stars = (n * 4) - 1;
        for (int i = 0; i < n * 2; i++) {
            int space = i;
            while (space > 0) {
                System.err.print(" ");
                space--;
            }
            for (int j = 0; j < stars; j++) {
                System.err.print("*");
            }
            stars -= 2;
            System.err.println();
        }
    }
}
