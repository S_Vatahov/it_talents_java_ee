/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task10 {

    public static void main(String[] args) {
        int n = 5;
        int inSpace = 1;
        for (int i = 0; i < n; i++) {
            int space = n - i;
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            if (i == 0 || i == 1 || i == n - 1) {
                for (int j = 0; j <= i; j++) {
                    System.out.print("*");
                }
            } else {
                System.out.print("*");
                for (int j = 0; j < inSpace; j++) {
                    System.out.print(" ");
                }
                System.out.print("*");
                inSpace++;
            }
            System.out.println();
        }
    }
}
