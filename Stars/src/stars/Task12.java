/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task12 {

    public static void main(String[] args) {
        int n = 5;
        int space = n - 3;
        for (int i = 0; i < n; i++) {
            if (i == 0 || i == n - 2 || i == n - 1) {
                for (int j = 0; j < n - i; j++) {
                    System.out.print("*");
                }
            } else {
                int currSpace = space;
                System.out.print("*");
                while (currSpace > 0) {
                    System.out.print(" ");
                    currSpace--;
                }
                System.out.print("*");
                space--;
            }
            System.out.println();
        }
    }

}
