/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task27 {

    public static void main(String[] args) {
        int n = 5;

        for (int i = 0; i < (n * 2) - 1; i++) {
            if (i == 0 || i == (n * 2 / 2) - 1 || i == (n * 2) - 2) {
                System.out.print(" ");
                for (int j = 0; j < n - 2; j++) {
                    System.out.print("*");
                }
            } else {
                System.out.print("*");
                for (int j = 0; j < n - 2; j++) {
                    System.out.print(" ");
                }
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
