/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task15 {

    public static void main(String[] args) {
        int n = 5;
        int stars = 1;
        for (int i = 0; i < n; i++) {
            int space = n - i;
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            for (int j = 0; j < stars; j++) {
                System.out.print("*");
            }
            stars += 2;
            System.out.println();
        }
    }
}
