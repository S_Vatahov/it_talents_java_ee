/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task17 {

    public static void main(String[] args) {
        int n = 5;
        int stars = (n * 2) - 1;
        for (int i = 0; i < n; i++) {
            int space = i;
            while (space > 0) {
                System.out.print(" ");
                space--;
            }
            for (int j = 0; j < stars; j++) {
                System.out.print("*");
            }
            stars -= 2;
            System.out.println();
        }
    }
}
