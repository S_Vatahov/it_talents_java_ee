/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task26 {

    public static void main(String[] args) {
        int n = 5;
        char arr[][] = new char[(n * 2) - 1][(n * 2) - 1];

        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = '*';
            arr[i][arr.length - i - 1] = '*';
        }
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                if (arr1[j] == '\u0000') {
                    System.out.print(" ");
                }
                System.out.print(arr1[j]);
            }
            System.out.println();
        }
    }
}
