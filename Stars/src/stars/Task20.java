/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stars;

/**
 *
 * @author slavi
 */
public class Task20 {

    public static void main(String[] args) {
        int n = 5;
        int index = n - 1;
        for (int i = 0; i < (n * 2) - 1; i++) {
            if (i < n) {
                int space = n - i;
                while (space > 0) {
                    System.out.print(" ");
                    space--;
                }
                for (int j = 0; j <= i; j++) {
                    System.out.print("*");
                }
            } else {
                int space = (n - index) + 1;
                while (space > 0) {
                    System.out.print(" ");
                    space--;
                }
                for (int j = 0; j < index; j++) {
                    System.out.print("*");
                }
                index--;
            }
            System.out.println();
        }
    }
}
