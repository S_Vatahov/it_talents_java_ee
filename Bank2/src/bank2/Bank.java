/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author slavi
 */
public class Bank {

    private String name;
    private String adress;
    private final Map<Client, ArrayList<Deposit>> depositMap;
    private final Map<Client, ArrayList<Credit>> creditMap;
    private double bankMoney;
    private double bankReserve;
    private double depositInterestRate;
    private double creditInterestRate;

    Bank(String name, String adress) {
        setName(name);
        setAdress(adress);
        depositMap = new HashMap<>();
        creditMap = new HashMap<>();
    }

    void acceptsADeposit(Client client, Deposit newDeposit) {
        newDeposit.setDepositAmountPaid(calculationOfInterest(newDeposit));
        this.getDepositMap().put(client, client.getMyDepositList());
        this.setBankMoney(this.getBankMoney() + newDeposit.getCash());
        this.setBankReserve(this.getBankReserve() + (newDeposit.getCash() * 90) / 100);
    }

    private double calculationOfInterest(Deposit deposit) {
        return deposit.getCash() * deposit.getPeriod() * (this.getDepositInterestRate() / 100);
    }

    void paymentOfInterestOnDeposits() {
        getDepositMap().entrySet().stream().map((entry) -> {
            final Client client = entry.getKey();
            final ArrayList<Deposit> depositList = entry.getValue();
            for (int i = 0; i < depositList.size(); i++) {
                getDepositMap().get(client).get(i).setCash(getDepositMap().get(client).get(i).getCash() + getDepositMap().get(client).get(i).getDepositAmountPaid());
            }
            return client;
        }).forEachOrdered((client) -> {
            getDepositMap().put(client, getDepositMap().get(client));
        });
    }

    boolean creditApproval(Client client, Credit newCredit) {
        ArrayList<Credit> clientCreditsList = this.getCreditMap().get(client);
        this.setCreditInstallment(newCredit);
        if (clientCreditsList == null) {
            if (client.getSalary() < (newCredit.getCreditInstallment() * 50) / 100) {
                System.err.println("You are not eligible for credit");
                return false;
            }
            if ((this.getBankReserve() - newCredit.getCash()) < (this.getTheAmountOfAllDeposits() * 10) / 100) {
                System.err.println("The bank can not grant this credit due to insufficient availability");
                return false;
            }
            clientCreditsList = new ArrayList<>();
            clientCreditsList.add(newCredit);
            this.creditMap.put(client, clientCreditsList);
            this.setBankReserve(this.getBankReserve() - newCredit.getCash());
            return true;
        }
        double sum = 0;
        for (int i = 0; i < clientCreditsList.size(); i++) {
            sum += clientCreditsList.get(i).getCreditInstallment();
        }
        if (client.getSalary() < ((sum + newCredit.getCreditInstallment()) * 50) / 100) {
            System.err.println("You are not eligible for credit");
            return false;
        }
        if ((this.getBankReserve() - newCredit.getCash()) < (this.getTheAmountOfAllDeposits() * 10) / 100) {
            System.err.println("The bank can not grant this credit due to insufficient availability");
            return false;
        }
        clientCreditsList.add(newCredit);
        this.creditMap.put(client, clientCreditsList);
        this.setBankReserve(this.getBankReserve() - newCredit.getCash());
        return true;
    }

    private double getTheAmountOfAllDeposits() {
        double sum = 0;
        for (Map.Entry<Client, ArrayList<Deposit>> entry : this.getDepositMap().entrySet()) {
            final ArrayList<Deposit> depositList = entry.getValue();
            for (int i = 0; i < depositList.size(); i++) {
                sum += depositList.get(i).getCash();
            }
        }
        return sum;
    }

    private void setCreditInstallment(Credit newCredit) {
        newCredit.setCreditInstallment(newCredit.getCash() * newCredit.getPeriod() * (this.getCreditInterestRate() / 100));
    }

    final void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid bank name");
            return;
        }
        this.name = name;
    }

    final void setAdress(String adress) {
        if (adress == null || adress.equals("")) {
            System.err.println("Invalid bank adress");
            return;
        }
        this.adress = adress;
    }

    void setBankReserve(double bankReserve) {
        this.bankReserve = bankReserve;
    }

    void setBankMoney(double bankMoney) {
        this.bankMoney = bankMoney;
    }

    void setDepositInterestRate(double depositInterestRate) {
        if (depositInterestRate < 0) {
            System.err.println("Invalid deposit interest rate");
            return;
        }
        this.depositInterestRate = depositInterestRate;
    }

    void setCreditInterestRate(double creditInterestRate) {
        if (creditInterestRate < 0) {
            System.err.println("Invalid credit interest rate");
            return;
        }
        this.creditInterestRate = creditInterestRate;
    }

    String getName() {
        return name;
    }

    String getAdress() {
        return adress;
    }

    double getBankMoney() {
        return bankMoney;
    }

    double getBankReserve() {
        return bankReserve;
    }

    double getDepositInterestRate() {
        return depositInterestRate;
    }

    double getCreditInterestRate() {
        return creditInterestRate;
    }

    Map<Client, ArrayList<Credit>> getCreditMap() {
        return creditMap;
    }

    Map<Client, ArrayList<Deposit>> getDepositMap() {
        return depositMap;
    }

}
