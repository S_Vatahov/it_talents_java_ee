/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;


/**
 *
 * @author slavi
 */
public abstract class BankProduct {

    private String name;
    private ProductType type;
    private int period;
    private double creditInstallment;
    private double depositAmountPaid;
    private double cash;

     final void setCash(double cashAvailability) {
        if (type == ProductType.DEPOSIT) {
            if (cashAvailability < 0) {
                System.err.println("Invalid deposit amount");
                return;
            }
        }
        this.cash = cashAvailability;
    }

    final void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid product name");
            return;
        }
        this.name = name;
    }

    final void setType(ProductType type) {
        this.type = type;
    }

     final void setPeriod(int period) {
        if (period < 1 || period > 60) {
            System.err.println("Invalid product period");
            return;
        }
        this.period = period;
    }

     final void setCreditInstallment(double creditInstallment) {
        if (creditInstallment < 0) {
            System.err.println("Invalid credit installment");
            return;
        }
        this.creditInstallment = creditInstallment;
    }

     final void setDepositAmountPaid(double depositAmountPaid) {
        if (depositAmountPaid < 0) {
            System.out.println("Invalid deposit amount paid");
            return;
        }
        this.depositAmountPaid = depositAmountPaid;
    }


      int getPeriod() {
        return period;
    }

     double getDepositAmountPaid() {
        return depositAmountPaid;
    }

     double getCreditInstallment() {
        return creditInstallment;
    }

    String getName() {
        return name;
    }

    double getCash() {
        return cash;
    }

}
