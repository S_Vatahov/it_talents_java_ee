/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

/**
 *
 * @author slavi
 */
public class Deposit extends BankProduct {

    public Deposit(String name, int period, double cash) {
        this.setType(ProductType.DEPOSIT);
        this.setName(name);
        this.setPeriod(period);
        this.setCash(cash);
    }

}
