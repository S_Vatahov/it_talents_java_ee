/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Client {

    private String name;
    private String adress;
    private double money;
    private double salary;
    private ArrayList<Deposit> depositList;
    private ArrayList<Credit> creditList;
    private Bank bank;

    Client(String name, String adress) {
        this.setName(name);
        this.setAdress(adress);
        this.setClientCreditList(new ArrayList<>());
        this.setClientDepositList(new ArrayList<>());
    }

    void opensADeposit(Deposit deposit) {
        if (this.getMoney() <= 0) {
            System.err.println("You have no money");
            return;
        }
        if (this.getMoney() < deposit.getCash()) {
            System.err.println("You do not have that much money");
            return;
        }
        if (this.getBank() == null) {
            System.err.println("Please select a bank");
            return;
        }
        this.getMyDepositList().add(deposit);
        this.getBank().acceptsADeposit(this, deposit);
        this.setMoney(this.getMoney() - deposit.getCash());
    }

    void getCreditFromBank(Credit newCredit) {
        if (newCredit == null) {
            System.err.println("Invalid credit");
            return;
        }
        if (this.getBank().creditApproval(this, newCredit)) {
            this.getMyCreditList().add(newCredit);
            this.setMoney(this.getMoney()+newCredit.getCash());
        }
    }

    final void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid client name");
            return;
        }
        this.name = name;
    }

    final void setAdress(String adress) {
        if (adress == null || adress.equals("")) {
            System.err.println("Invalid client adress");
            return;
        }
        this.adress = adress;
    }

    void setMoney(double money) {
        if (money < 0) {
            System.err.println("Invalid client money");
            return;
        }
        this.money = money;
    }

    void setSalary(double salary) {
        if (salary < 0) {
            System.err.println("Invalid client salary");
            return;
        }
        this.salary = salary;
    }

    final void setClientDepositList(ArrayList<Deposit> clientDepositList) {
        if (clientDepositList == null) {
            System.err.println("Invalid client deposit list");
            return;
        }
        this.depositList = clientDepositList;
    }

    final void setClientCreditList(ArrayList<Credit> clientCreditList) {
        if (clientCreditList == null) {
            System.err.println("Invalid client credit list");
            return;
        }
        this.creditList = clientCreditList;
    }

    void setBank(Bank bank) {
        if (bank == null) {
            System.err.println("Invalid bank");
            return;
        }
        this.bank = bank;
    }

    ArrayList<Deposit> getMyDepositList() {
        return depositList;
    }

    ArrayList<Credit> getMyCreditList() {
        return creditList;
    }

    double getMoney() {
        return money;
    }

    private Bank getBank() {
        return bank;
    }

    String getName() {
        return name;
    }

    String getAdress() {
        return adress;
    }

    double getSalary() {
        return salary;
    }

    String getBankName() {
        return this.getBank().getName();
    }

    String getBankAdress() {
        return this.getBank().getAdress();
    }

}
