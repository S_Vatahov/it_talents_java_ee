/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank2;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Demo {

    public static void main(String[] args) {
        Bank bulgaria = new Bank("Bulgaria", "Sofia");
        bulgaria.setCreditInterestRate(4.5);
        bulgaria.setDepositInterestRate(1.5);
        bulgaria.setBankMoney(50000);
        bulgaria.setBankReserve(50000);

        Client puhi = new Client("Puhi", "Sofia");
        puhi.setMoney(1000000);
        puhi.setSalary(12000);
        puhi.setBank(bulgaria);
        puhi.opensADeposit(new Deposit("Depozita na Puhi", 12, 150000));
        // puhi.getCreditFromBank(new Credit("puhi", 12, 100));
        puhi.getCreditFromBank(new Credit("c1", 12, 100));
        puhi.getCreditFromBank(new Credit("c2", 12, 200));
        puhi.getCreditFromBank(new Credit("c3", 12, 300));
        puhi.getCreditFromBank(new Credit("c4", 12, 400));

        System.out.println(bulgaria.getBankReserve());
        System.out.println(bulgaria.getBankMoney());

        ArrayList<Credit> aa = puhi.getMyCreditList();
        for (int i = 0; i < aa.size(); i++) {
            System.out.println(aa.get(i).getCreditInstallment());
        }

    }
}
