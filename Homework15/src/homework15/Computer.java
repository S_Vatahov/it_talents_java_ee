/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework15;

/**
 *
 * @author slavi
 */
public class Computer {

    private int year;
    private double hardDiskMemory;
    private double freeMemory;
    private double price;
    private String computerType;
    private String operationSystem;

    public Computer() {
        this.computerType = ComputerType.DESKTOP.name();
        this.operationSystem = OperationSystem.WINDOWS.name();
    }

    public Computer(int year, double price, double hardDiskMemory, double freeMemory) {
        this();

        if (year > 1960) {
            this.year = year;
        } else {
            System.err.println("Enter correct year!");
        }
        if (price > 0) {
            this.price = price;
        } else {
            System.err.println("Enter correct price!");
        }
        if (hardDiskMemory > 1) {
            this.hardDiskMemory = hardDiskMemory;
        } else {
            System.err.println("Enter correct hard disk memory!");
        }
        if (freeMemory > 0 && freeMemory < hardDiskMemory) {
            this.freeMemory = freeMemory;
        } else {
            System.err.println("Enter correct free memory!");
        }
    }

    public Computer(int year, double price, int hardDiskMemory, double freeMemory, ComputerType computerType, OperationSystem operationSystem) {
        this(year, price, hardDiskMemory, freeMemory);

        this.operationSystem = operationSystem.name();
        this.computerType = computerType.name();
    }

    public void changeOperationSystem(OperationSystem newOperationSystem) {
        this.operationSystem = newOperationSystem.name();
    }

    public void useMemory(double memory) {
        if (memory < 0) {
            System.err.println("Incorrect data");
            return;
        }
        if (this.freeMemory < memory) {
            System.err.println("Not enough free memory!");
            return;
        }
        this.freeMemory -= memory;
    }

    public void printState() {
        System.out.println("Year " + this.year);
        System.out.println("Hard disk memory  " + this.hardDiskMemory + " GB");
        System.out.println("Free memory  " + this.freeMemory + " GB");
        System.out.println("Price " + this.price + " $");
        System.out.println("Computer type  " + this.computerType);
        System.out.println("Operation system " + this.operationSystem);
    }

}
