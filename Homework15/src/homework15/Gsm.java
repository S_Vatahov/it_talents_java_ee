/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework15;

/**
 *
 * @author slavi
 */
public class Gsm {

    private final String model;
    private String simMobileNumber;
    private Call lastIncomingCall;
    private Call lastOutgoingCall;

    private double outgoingCallsDuration;
    private boolean hasSimCard;

    public Gsm(String model) {
        this.model = model;
    }

    public void insertSimCard(String simCardNumber) {
        if (!validatePhoneNumber(simCardNumber)) {
            System.err.println("Invalid number");
            return;
        }
        this.simMobileNumber = simCardNumber;
        this.hasSimCard = true;
    }

    private static boolean validatePhoneNumber(String phoneNo) {

        return phoneNo.matches("\\d{10}") && phoneNo.charAt(0) == '0';
    }

    public void removeSimCard() {
        this.hasSimCard = false;
        this.simMobileNumber = null;
    }

    public void call(Gsm receiver, int duration) {
        if (duration > 500 || duration < 0) {
            System.err.println("Invalid duration");
            return;
        }
        if (!this.hasSimCard) {
            System.err.println("Sim card ERROR");
            return;
        }
        if (!receiver.hasSimCard) {
            System.err.println(receiver.model + " sim card ERROR");
            return;
        }
        if (receiver.simMobileNumber.equals(this.simMobileNumber)) {
            System.err.println("You can not call yourself!");
            return;
        }

        this.lastOutgoingCall = new Call(simMobileNumber, receiver.simMobileNumber, duration);
        receiver.lastIncomingCall = new Call(receiver.simMobileNumber, this.simMobileNumber, duration);
        this.outgoingCallsDuration += duration;
    }

    public void printInfoForTheLastOutgoingCall() {
        if (lastOutgoingCall == null) {
            System.err.println("No Outgoing Call!");
            return;
        }
        System.out.println(this.model + " last out going call " + lastOutgoingCall.reciver);
    }

    public void printInfoForTheLastIncomingCall() {
        if (lastIncomingCall == null) {
            System.err.println("No Incoming  Call!");
            return;
        }
        System.out.println(this.model + " last incoming call " + lastIncomingCall.reciver);
    }

    public double getSumForCal() {
        return outgoingCallsDuration * Call.priceForAMinute;
    }
}
