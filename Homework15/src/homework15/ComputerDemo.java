/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework15;

/**
 *
 * @author slavi
 */
public class ComputerDemo {

    public static void main(String[] args) {
        Computer pc1 = new Computer(2017, 2000, 2000, 1500, ComputerType.NOTEBOOK, OperationSystem.LINUX);
        Computer pc2 = new Computer(2017, 2000, 3000, 2500, ComputerType.DESKTOP, OperationSystem.LINUX);

        pc1.useMemory(1501);
        pc2.changeOperationSystem(OperationSystem.CHORMEOS);
        
        

        pc1.printState();
        System.out.println();
        pc2.printState();

    }
}
