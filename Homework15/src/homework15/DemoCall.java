/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework15;

/**
 *
 * @author slavi
 */
public class DemoCall {

    public static void main(String[] args) {
        Gsm iPhone = new Gsm("iPhone");
        Gsm nokia = new Gsm("Nokia");
        Gsm lg = new Gsm("LG");

        iPhone.insertSimCard("0899476316");
        nokia.insertSimCard("0899476315");
        lg.insertSimCard("0899476314");

        iPhone.call(nokia, 500);
        iPhone.call(nokia, 305);
        nokia.call(iPhone, 3);
        nokia.call(lg, 500);
        
        
        nokia.removeSimCard();
        iPhone.call(nokia, 10);

        
        
        iPhone.printInfoForTheLastIncomingCall();
        iPhone.printInfoForTheLastOutgoingCall();

        nokia.printInfoForTheLastIncomingCall();
        nokia.printInfoForTheLastOutgoingCall();

        System.out.printf("%.2f" + "%s", nokia.getSumForCal(), "$\n");
        System.out.printf("%.2f" + "%s", iPhone.getSumForCal(), "$\n");

    }
}
