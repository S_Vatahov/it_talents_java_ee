/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static int multiplication(int[][] arr) {
        int multi = 1;
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(arr[i][j] + (j == i - 1 && i == arr.length - 1 ? "=" : "*"));
                multi *= arr[i][j];
            }
        }
        return multi;
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 16}
        };
        System.out.println(multiplication(arr));
    }
}
