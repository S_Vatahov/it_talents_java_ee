/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task2 {

    private static boolean isZigZag(int[] arr, int index) {
        if (index == arr.length - 1) {
            return true;
        }
        if (((index % 2 == 0) && (arr[index] > arr[index - 1] || arr[index] > arr[index + 1])) || (index % 2 != 0) && (arr[index] < arr[index - 1] || arr[index] < arr[index + 1])) {
            return false;
        }
        return isZigZag(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 2, 4, 3, 7};
        System.out.println(isZigZag(arr, 1));
    }
}
