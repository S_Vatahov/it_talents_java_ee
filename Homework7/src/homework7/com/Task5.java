/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task5 {
    
    private static int maxSum(int[][] matrix) {
        int curSum = 0;
        int maxSum = 0;
        int row = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                curSum += matrix[i][j];
            }
            if (maxSum < curSum) {
                maxSum = curSum;
                row = i;
            }
            curSum = 0;
        }
        System.out.println("The max sum is " + maxSum);
        return row;
    }
    
    public static void main(String[] args) {
        int[][] matrix = {
            {1, 2, 3, 4, 5},
            {2, 22, 5, 1, 1},
            {1, 2, 3, 44, 5},
            {18, 12, 3, 4, 5}
        };
        System.out.println("On row " + maxSum(matrix));
    }
}
