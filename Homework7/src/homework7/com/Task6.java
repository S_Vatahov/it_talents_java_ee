/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task6 {

    private static int printDiagonals(char[][] matrix, int index) {
        if (index == matrix.length) {
            return -1;
        }
        System.out.print(matrix[index][index] + "|");
        return printDiagonals(matrix, index + 1);
    }

    public static void main(String[] args) {
        char[][] matrix = {
            {'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h'},
            {'i', 'j', 'k', 'l'},
            {'m', 'n', 'o', 'p'}
        };
        printDiagonals(matrix, 0);
    }
}
