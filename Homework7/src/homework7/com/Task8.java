/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task8 {

    private static boolean containsTrue(boolean[][] arr) {
        for (int i = 0; i < arr.length - i - 1; i++) {
            for (int j = 0; j < arr.length - j - 1; j++) {
                if (arr[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        boolean[][] arr = {
            {false, false, false, false},
            {false, true, false, false},
            {false, false, false, false},
            {false, false, false, false}
        };
        System.out.println("Contains ? - " + containsTrue(arr));
    }
}
