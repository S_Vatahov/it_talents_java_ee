/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

import java.util.Scanner;


/**
 *
 * @author slavi
 */
public class Task11 {

    private static void fillArrayA(int[][] matrix) {
        int numb = 1;
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix1.length; j++) {
                matrix1[j] = numb++;
            }
        }
        printMatrix(matrix);
    }

    private static void fillArrayB(int[][] matrix) {
        int numb = 1;
        for (int j = 0; j < matrix[0].length; j++) {
            for (int[] matrix1 : matrix) {
                matrix1[j] = numb++;
            }
        }
        printMatrix(matrix);
    }

    private static void fillArrayC(int[][] matrix) {
        boolean bootomI = false;
        boolean rightJ = false;
        int numb = 1;
        int indexI = 0;
        int indexJ = 0;
        while (!bootomI || !rightJ) {
            int curI = indexI;
            int curJ = indexJ;
            if (!bootomI) {
                for (int i = indexI; i >= 0 && curJ < matrix[0].length; i--) {
                    matrix[i][curJ++] = numb++;
                }
                if (indexI + 1 == matrix.length) {
                    bootomI = true;
                    indexJ++;
                } else {
                    indexI++;
                }
                continue;
            }
            if (!rightJ) {

                for (int i = indexI; i >= 0 && curJ < matrix[0].length; i--) {
                    matrix[i][curJ++] = numb++;
                }
                if (indexJ + 1 == matrix[0].length) {
                    rightJ = true;
                } else {
                    indexJ++;
                }
            }
        }
        printMatrix(matrix);
    }

    private static void fillArrayD(int[][] matrix) {
        int numb = 1;
        boolean downUp = false;
        int indexI = 0;
        int indexJ = 0;
        while (numb < matrix.length * matrix[0].length) {

            while (!downUp) {
                matrix[indexI][indexJ] = numb++;
                indexI++;
                if (indexI == matrix.length) {
                    indexI--;
                    indexJ = indexJ + 1;
                    downUp = true;
                }
            }
            if (numb >= matrix.length * matrix[0].length) {
                break;
            }
            while (downUp) {
                matrix[indexI][indexJ] = numb++;
                indexI--;
                if (indexI == -1) {
                    indexI++;
                    indexJ = indexJ + 1;
                    downUp = false;
                }
            }
            if (numb >= matrix.length * matrix[0].length) {
                break;
            }
        }
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix1.length; j++) {
                System.out.print(matrix1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            int row, col;
            do {
                System.out.print("Enter row ");
                row = input.nextInt();
            } while (row <= 0);
            do {
                System.out.print("Enter col ");
                col = input.nextInt();
            } while (col <= 0);
            int[][] matrix = new int[row][col];
            System.out.println("Enter a,b,c or d");
            char a = input.next().charAt(0);
            switch (a) {
                case 'a':
                    fillArrayA(matrix);
                    break;
                case 'b':
                    fillArrayB(matrix);
                    break;
                case 'c':
                    fillArrayC(matrix);
                    break;
                case 'd':
                    fillArrayD(matrix);
                    break;
                default:
                    System.err.println("Worng input");
                    break;
            }
        }
    }
}
