/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static boolean isPositive(int[] arr, int index) {
        if (index == arr.length) {
            return true;
        }
        if (arr[index] < 0) {
            return false;

        }
        return isPositive(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, -5, 6};
        System.out.println("Is positive - " + isPositive(arr, 0));
    }

}
