/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task9 {

    private static int[][] maxSubMatrix(int[][] matrix) {
        int[][] subMatrix = new int[2][2];

        int maxSum = 0;
        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix.length - 1; j++) {

                int currSum = matrix[i][j] + matrix[i][j + 1] + matrix[i + 1][j] + matrix[i + 1][j + 1];

                if (maxSum < currSum) {
                    maxSum = currSum;
                    subMatrix[0][0] = matrix[i][j];
                    subMatrix[0][1] = matrix[i][j + 1];
                    subMatrix[1][0] = matrix[i + 1][j];
                    subMatrix[1][1] = matrix[i + 1][j + 1];
                }
            }
        }
        return subMatrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {
            {1, 2, 3, 4},
            {5, 6, 11, 2},
            {1, 22, 3, 4},
            {1, 1, 31, 4},};
        printMatrix(maxSubMatrix(matrix));
    }
}
