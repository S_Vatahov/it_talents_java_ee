/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task4 {
    
    private static void sortArr(int[] arr) {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                counter++;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (i < counter) {
                arr[i] = 0;
            } else {
                arr[i] = 1;
            }
        }
    }
    
    private static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }
    }
    
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 0, 0, 1, 1, 0, 1};
        sortArr(arr);
        printArr(arr);
    }
}
