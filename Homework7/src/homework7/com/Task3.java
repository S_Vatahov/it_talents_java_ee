/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static int contains(char[] arr, int index, char ch) {
        if (index == arr.length) {
            return -1;
        }
        if (arr[index] == ch) {
            return index;
        }
        return contains(arr, index + 1, ch);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter character ");
        char ch = input.next().charAt(0);
        char[] arr = {'a', 'b', 'c',};
        int i = contains(arr, 0, ch);
        System.out.println(i == -1 ? "The item is not contained " : "Index of this element is " + i);
    }
}
