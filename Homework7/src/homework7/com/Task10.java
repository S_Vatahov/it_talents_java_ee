/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.com;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static void printTheMostCommonElement(int[] arr) {
        int maxCounter = 0;
        int element = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            int currCounter = 1;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    currCounter++;
                }
            }
            if (currCounter > maxCounter) {
                maxCounter = currCounter;
                element = arr[i];
            }
        }
        System.out.println("The most common element is =>" + element + " (" + maxCounter + " times)");
    }

    public static void main(String[] args) {
        int[] arr = {4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3};
        printTheMostCommonElement(arr);
    }
}
