/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.Random;

/**
 *
 * @author slavi
 */
public class Judge extends LegalEntity {

    public Judge(String name, String adress, int age, int internship, int numberOfCases) {
        super(name, adress, age, internship, numberOfCases);
    }

    int determinationOfSentence() {
        return new Random().nextInt(8) + 3;
    }
}
