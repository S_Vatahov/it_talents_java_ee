/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author slavi
 */
public abstract class CourtCase {

    private Judge judge;
    private Set<JudicialAssessor> judicialAssessor;
    private Accused accused;
    private Accuser accuser;
    private Prosecutor prcuror;
    private Set<Witness> witness;
    private StringBuilder builder;
    private static int counter = 1;

    public CourtCase(Judge judge, Accused accused, Accuser accuser, Set<JudicialAssessor> judicialAssessors, Set<Witness> witnesses) {
        this.setJudge(judge);
        this.setAccused(accused);
        this.setAccuser(accuser);
        this.setJudicialAssessor(judicialAssessors);
        this.setWitness(witnesses);
    }

    public CourtCase(Judge judge, Accused accused, Prosecutor prosecutor, Set<JudicialAssessor> judicialAssessors, Set<Witness> witnesses) {
        this.setJudge(judge);
        this.setAccused(accused);
        this.setProcuror(prosecutor);
        this.setJudicialAssessor(judicialAssessors);
        this.setWitness(witnesses);
    }

    private void setProcuror(Prosecutor procuror) {
        if (procuror == null) {
            System.out.println("Invalid Prokuror");
            return;
        }
        this.prcuror = procuror;
    }

    private void setJudge(Judge judge) {
        if (judge == null) {
            System.err.println("Invalid judge");
            return;
        }
        this.judge = judge;
    }

    private void setAccused(Accused obvinqem) {
        if (obvinqem == null) {
            System.err.println("Invalid accused");
            return;
        }
        this.accused = obvinqem;
    }

    private void setAccuser(Accuser obvinitel) {
        if (obvinitel == null) {
            System.err.println("Invalid Accuser");
            return;
        }
        this.accuser = obvinitel;
    }

    private void setJudicialAssessor(Set<JudicialAssessor> judicialAssessor) {
        this.judicialAssessor = judicialAssessor;
    }

    private void setWitness(Set<Witness> witness) {
        this.witness = witness;
    }

    void Perform() {
        builder = new StringBuilder();

        judge.setNumberOfCases(judge.getNumberOfCases() + 1);
        Iterator<JudicialAssessor> judicalIterator = judicialAssessor.iterator();
        while (judicalIterator.hasNext()) {
            JudicialAssessor ja = judicalIterator.next();
            ja.setNumberOfCases(ja.getNumberOfCases() + 1);
        }
        Iterator<Advokat> accusedAdvokatiIterator = accused.getAdvokati().iterator();
        while (accusedAdvokatiIterator.hasNext()) {
            Advokat ad = accusedAdvokatiIterator.next();
            ad.setNumberOfCases(ad.getNumberOfCases() + 1);
        }
        if (this instanceof CivilianCourtCase) {
            Iterator<Advokat> accuserAdvokatiIterator = accuser.getAdvokati().iterator();
            while (accuserAdvokatiIterator.hasNext()) {
                Advokat ad = accuserAdvokatiIterator.next();
                ad.setNumberOfCases(ad.getNumberOfCases() + 1);
            }
        } else if (this instanceof CriminalCourtCase) {
            prcuror.setNumberOfCases(prcuror.getNumberOfCases() + 1);
        }
        askQuestions();
        theAttorneysLawyerAsksQuestions();
        if (judicialAssessorGuiltyOrNot()) {
            builder.append("Vinoven polu4ava6 ").append(judge.determinationOfSentence()).append(" godini").append("\n");

        } else {
            builder.append("Nevinen").append("\n");
        }

        createFile();
    }

    private void askQuestions() {
        if (this instanceof CivilianCourtCase) {
            accuserAskQuestions();
        } else if (this instanceof CriminalCourtCase) {
            prosecutorAskQuestions();
        }

    }

    void prosecutorAskQuestions() {
        for (int i = 0; i < 5; i++) {
            builder.append(i).append("-vi vapros na ").append(accused.name).append("\n");
        }
        Iterator<Witness> witnessIterator = witness.iterator();
        while (witnessIterator.hasNext()) {
            Witness witnesss = witnessIterator.next();
            for (int i = 0; i < 5; i++) {
                builder.append(i).append("-vi vapros na ").append(witnesss.name).append("\n");
            }

        }
    }

    void accuserAskQuestions() {
        Iterator<Advokat> accuserAdvokatiIterator = accuser.getAdvokati().iterator();
        while (accuserAdvokatiIterator.hasNext()) {
            Advokat advokat = accuserAdvokatiIterator.next();
            builder.append("Advokat ").append(advokat.getName()).append("\n");
            for (int i = 0; i < 3; i++) {
                builder.append(i).append("-vi vapros na ").append(accused.name).append("\n");
            }
            Iterator<Witness> witnessIterator = witness.iterator();
            while (witnessIterator.hasNext()) {
                Witness witnesss = witnessIterator.next();
                for (int i = 0; i < 2; i++) {
                    builder.append(i).append("-vi vapros na ").append(witnesss.name).append("\n");
                }

            }
        }
    }

    void theAttorneysLawyerAsksQuestions() {
        Iterator<Advokat> accusedAdvokatiIterator = accused.getAdvokati().iterator();
        while (accusedAdvokatiIterator.hasNext()) {
            Advokat ad = accusedAdvokatiIterator.next();
            Iterator<Witness> witnessIterator = witness.iterator();
            builder.append("Advokat ").append(ad.getName()).append("\n");
            while (witnessIterator.hasNext()) {
                Witness witnesss = witnessIterator.next();
                for (int i = 0; i < 5; i++) {
                    builder.append(i).append("-vi vapros na ").append(witnesss.name).append("\n");
                }

            }
        }
    }

    private boolean judicialAssessorGuiltyOrNot() {
        ArrayList<Integer> vot = new ArrayList();

        Iterator<JudicialAssessor> iterator = judicialAssessor.iterator();
        while (iterator.hasNext()) {
            vot.add(iterator.next().generateRandumVot());
        }
        int z = 0;
        for (int i = 0; i < vot.size(); i++) {
            z += vot.get(i);
        }

        return z >= judicialAssessor.size() / 2;
    }

    void writeFile(String text, String path) {

        byte data[] = text.getBytes();
        Path p = Paths.get(path);

        try (OutputStream out = new BufferedOutputStream(
                Files.newOutputStream(p, CREATE, APPEND))) {
            out.write(data, 0, data.length);
            out.close();
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    void createFile() {
        try {
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            File dir = new File(this.getClass().getClassLoader().getResource("resources/").getPath());
            File newFile = new File(dir, "Delo " + (CourtCase.counter++) + " | " + timeStamp + " .txt");
            newFile.createNewFile();
            String path = newFile.getCanonicalPath();
            newFile.delete();
            writeFile(builder.toString(), path);
            System.out.println(newFile.getCanonicalPath());

        } catch (IOException e) {
        }
    }
}
