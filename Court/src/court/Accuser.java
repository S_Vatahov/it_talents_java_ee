/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author slavi
 */
public class Accuser extends Citizen {

    private Set<Advokat> advokati;

    public Accuser(String name, String adress, int age, HashSet<Advokat> advokati) {
        super(name, adress, age);
        this.setAdvokati(advokati);
    }

     Set<Advokat> getAdvokati() {
        return advokati;
    }

     final void setAdvokati(Set<Advokat> advokati) {
        this.advokati = advokati;
    }

}
