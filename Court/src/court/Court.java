/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author slavi
 */
public class Court {

    private String name;
    private String adress;
    private final Set<LegalEntity> legalEntities;
    private final Set<CourtCase> courtCases;

    public Court(String name, String adress) {
        this.setName(name);
        this.setAdress(adress);
        legalEntities = new HashSet<>();
        courtCases = new HashSet<>();
    }

    private void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid name");
            this.name = "Unnamed";
            return;
        }
        this.name = name;
    }

    private void setAdress(String adress) {
        if (adress == null || adress.equals("")) {
            System.err.println("Invalid adress");
            this.adress = "No address";
        }
        this.adress = adress;
    }

    Set<LegalEntity> getLegalEntities() {
        return legalEntities;
    }

    Set<CourtCase> getCourtCases() {
        return courtCases;
    }

}
