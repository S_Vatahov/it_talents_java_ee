/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.Objects;
import java.util.Set;

/**
 *
 * @author slavi
 */
public abstract class Citizen {

    protected String name;
    protected String adress;
    protected int age;
   

    public Citizen(String name, String adress, int age) {
        this.setName(name);
        this.setAge(age);
        this.setAdress(adress);
    }

    private void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid name");
            this.name = "Unnamed";
            return;
        }
        this.name = name;
        this.name = name;
    }

    private void setAdress(String adress) {
        if (adress == null || adress.equals("")) {
            System.err.println("Invalid adress");
            this.adress = "No adress";
            return;
        }
        this.adress = adress;
    }

    private void setAge(int age) {
        if (age < 20 || age > 100) {
            System.err.println("Invalid age");
            return;
        }
        this.age = age;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.adress);
        hash = 53 * hash + this.age;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Citizen other = (Citizen) obj;
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.adress, other.adress);
    }

}
