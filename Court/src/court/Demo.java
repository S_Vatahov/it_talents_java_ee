/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author slavi
 */
public final class Demo {

    static boolean isAdd;
    private static final String[] NAMES = {"Ivan", "Gosho", "Pesho", "Petkan", "Ginka", "Puhi", "Boncho", "Dragan", "Maslin",};
    private static final String[] ADRESES = {"Sofia", "Plovdiv", "Varna", "Veliko Tarnovo", "Burgas", "Gabrovo", "Ruse", "Blagoevgrad", "Goce Delchev",};

    private static void registrationOfLegalEntities(Court court, int judges, int judicialAssessors, int advokati, int prosecutor) {

        for (int i = 0; i < judges; i++) {
            isAdd = court.getLegalEntities().add(new Judge(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(1000)));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < judicialAssessors; i++) {
            isAdd = court.getLegalEntities().add(new JudicialAssessor(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(1000)));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < advokati; i++) {
            isAdd = court.getLegalEntities().add(new Advokat(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(1000)));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < prosecutor; i++) {
            isAdd = court.getLegalEntities().add(new Prosecutor(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(1000)));
            if (!isAdd) {
                i--;
            }
        }
    }

    private static HashSet<Citizen> registrationOfCitizens(int accused, int accuser, int witness) {
        Set<Citizen> citzens = new HashSet<>();
        HashSet<Advokat> advokati = new HashSet<>();
        for (int i = 0; i < accused; i++) {
            int r = new Random().nextInt(4);
            for (int j = 0; j < r; j++) {
                isAdd = advokati.add(new Advokat(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(100)));
                if (!isAdd) {
                    j--;
                }

            }
            isAdd = citzens.add(new Accused(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, advokati));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < accuser; i++) {
            int r = new Random().nextInt(4);
            for (int j = 0; j < r; j++) {
                isAdd = advokati.add(new Advokat(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, new Random().nextInt(40), new Random().nextInt(100)));
                if (!isAdd) {
                    j--;
                }

            }
            isAdd = citzens.add(new Accuser(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20, advokati));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < witness; i++) {
            isAdd = citzens.add(new Witness(NAMES[new Random().nextInt(NAMES.length)], ADRESES[new Random().nextInt(ADRESES.length)], new Random().nextInt(80) + 20));
            if (!isAdd) {
                i--;
            }
        }

        return (HashSet< Citizen>) citzens;
    }

    private static CourtCase registrationCourtCase(Court court, HashSet<Citizen> citizens, CourtType courtType) {
        Accuser accuser = null;
        Prosecutor prosecutor = null;
        int n;
        if (courtType.equals(CourtType.CIVIL)) {
            n = 3;
            accuser = getAccuser(citizens);
        } else {
            n = 10;
            prosecutor = getProsecutor(court);
        }

        Judge judge = getJudge(court);
        HashSet<JudicialAssessor> judicialAssessors = new HashSet<>(n);
        Accused accused = getAccused(citizens);

        int numberOfWitness = new Random().nextInt(5);
        HashSet<Witness> witnesses = new HashSet<>(numberOfWitness);

        for (int i = 0; i < n; i++) {
            isAdd = judicialAssessors.add(getJudicialAssessor(court));
            if (!isAdd) {
                i--;
            }
        }
        for (int i = 0; i < numberOfWitness; i++) {
            isAdd = witnesses.add(getWitness(citizens));
            if (!isAdd) {
                i--;
            }
        }
        if (courtType.equals(CourtType.CIVIL)) {
            return new CivilianCourtCase(judge, accused, accuser, judicialAssessors, witnesses);
        }
        return new CriminalCourtCase(judge, accused, prosecutor, judicialAssessors, witnesses);

    }

    private static Judge getJudge(Court court) {

        Iterator<LegalEntity> iterator = court.getLegalEntities().iterator();
        int r = new Random().nextInt(court.getLegalEntities().size());
        int i = 0;
        while (iterator.hasNext()) {
            LegalEntity le = iterator.next();
            if (i == r) {
                if (le instanceof Judge) {
                    return (Judge) le;
                }
            }
            i++;
        }
        return getJudge(court);
    }

    private static Accused getAccused(HashSet<Citizen> citzens) {
        Iterator<Citizen> iterator = citzens.iterator();
        int r = new Random().nextInt(citzens.size());
        int i = 0;
        while (iterator.hasNext()) {
            Citizen citzen = iterator.next();
            if (i == r) {
                if (citzen instanceof Accused) {
                    return (Accused) citzen;
                }
            }
            i++;
        }
        return getAccused(citzens);
    }

    private static Prosecutor getProsecutor(Court court) {
        Iterator<LegalEntity> iterator = court.getLegalEntities().iterator();
        int r = new Random().nextInt(court.getLegalEntities().size());
        int i = 0;
        while (iterator.hasNext()) {
            LegalEntity citzen = iterator.next();
            if (i == r) {
                if (citzen instanceof Prosecutor) {
                    return (Prosecutor) citzen;
                }
            }
            i++;
        }
        return getProsecutor(court);

    }

    private static Accuser getAccuser(HashSet<Citizen> citzens) {
        Iterator<Citizen> iterator = citzens.iterator();
        int r = new Random().nextInt(citzens.size());
        int i = 0;
        while (iterator.hasNext()) {
            Citizen citzen = iterator.next();
            if (i == r) {
                if (citzen instanceof Accuser) {
                    return (Accuser) citzen;
                }
            }
            i++;
        }
        return getAccuser(citzens);

    }

    private static JudicialAssessor getJudicialAssessor(Court court) {

        Iterator<LegalEntity> iterator = court.getLegalEntities().iterator();
        int r = new Random().nextInt(court.getLegalEntities().size());
        int i = 0;
        while (iterator.hasNext()) {
            LegalEntity ja = iterator.next();
            if (i == r) {
                if (ja instanceof JudicialAssessor) {
                    return (JudicialAssessor) ja;
                }
            }
            i++;
        }
        return getJudicialAssessor(court);

    }

    private static Witness getWitness(HashSet<Citizen> citzens) {

        Iterator<Citizen> iterator = citzens.iterator();
        int r = new Random().nextInt(citzens.size());
        int i = 0;
        while (iterator.hasNext()) {
            Citizen citzen = iterator.next();
            if (i == r) {
                if (citzen instanceof Witness) {
                    return (Witness) citzen;
                }
            }
            i++;
        }
        return getWitness(citzens);
    }

    private static void addCourtCaseInCourt(Court court, HashSet<Citizen> citizens, int n, CourtType courtType) {
        for (int i = 0; i < n; i++) {
            isAdd = court.getCourtCases().add(registrationCourtCase(court, citizens, courtType));
            if (!isAdd) {
                i--;
            }
        }
    }

    private static void runAllCourtCases(Court court) {
        Iterator<CourtCase> cc = court.getCourtCases().iterator();
        while (cc.hasNext()) {
            cc.next().Perform();
        }
    }

    public static void main(String[] args) {
        Demo d = new Demo();
        Court velikoTarnovo = new Court("Veliko Tarnovo", "Veliko Tarnovo");
        registrationOfLegalEntities(velikoTarnovo, 3, 10, 5, 3);
        HashSet<Citizen> citizens = registrationOfCitizens(5, 5, 10);
        addCourtCaseInCourt(velikoTarnovo, citizens, 3, CourtType.CIVIL);
        addCourtCaseInCourt(velikoTarnovo, citizens, 3, CourtType.CRIMINAL);
        runAllCourtCases(velikoTarnovo);

    }
}
