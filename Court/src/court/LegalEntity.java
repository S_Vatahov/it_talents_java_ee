/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package court;

import java.util.Objects;

/**
 *
 * @author slavi
 */
public abstract class LegalEntity {
    
    private String name;

    public String getName() {
        return name;
    }
    private int internship;
    private int numberOfCases;
    
    public LegalEntity(String name, String adress, int age, int internship, int numberOfCases) {
        this.setName(name);
        this.setInternship(internship);
        this.setNumberOfCases(numberOfCases);
    }
    
    private void setInternship(int internship) {
        if (internship < 0) {
            System.err.println("Invalid internship");
            return;
        }
        this.internship = internship;
    }
    
    final void setNumberOfCases(int numberOfCases) {
        if (numberOfCases < 0) {
            System.err.println("Invalid numberOfCases");
            return;
        }
        this.numberOfCases = numberOfCases;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + this.internship;
        hash = 71 * hash + this.numberOfCases;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LegalEntity other = (LegalEntity) obj;
        if (this.internship != other.internship) {
            return false;
        }
        if (this.numberOfCases != other.numberOfCases) {
            return false;
        }
        return Objects.equals(this.name, other.name);
    }
    
    int getNumberOfCases() {
        return numberOfCases;
    }

    private void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Invalid name");
            this.name = "Unnamed";
            return;
        }
        this.name = name;
        this.name = name;
    }
}
