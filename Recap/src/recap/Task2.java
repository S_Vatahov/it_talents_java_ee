/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recap;

/**
 *
 * @author slavi
 */
public class Task2 {

    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = s1;
        String s5 = "abc";
        String s3 = new String("abc");
        String s4 = new String("abc");
        System.out.println("==  " + (s1 == s5));
        System.out.println("==  " + (s1 == s2));
        System.out.println("equals  " + (s1.equals(s2)));
        System.out.println("==  " + (s3 == s4));
        System.out.println("equals  " + (s3.equals(s4)));
    }
}
