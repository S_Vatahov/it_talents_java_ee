/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recap;

/**
 *
 * @author slavi
 */
public class Task4 {

    public static void main(String[] args) {
        int[] a = {1, 3, 5, 7, 9};
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < 3; j++) {
                if (i > 4 && i < 8) {
                    continue;
                }
                System.out.print(" " + i);
                if (j == 1) {
                    break;
                }
                continue;
            }
            continue;
        }
    }
}
