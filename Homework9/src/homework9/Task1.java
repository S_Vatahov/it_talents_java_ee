/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework9;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static int fibunachi(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibunachi(n - 1) + fibunachi(n - 2);
    }

    public static void main(String[] args) {
        System.out.println(fibunachi(6));
    }
    
}
