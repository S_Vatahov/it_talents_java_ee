/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework9;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static boolean isPrime(int number, int divisor) {
        if (divisor > 7) {
            return true;
        }
        if (number % divisor == 0 && divisor != number) {
            return false;
        }
        return isPrime(number, divisor + 1);
    }

    public static void main(String[] args) {
        System.out.println(isPrime(101, 2));
    }
}
