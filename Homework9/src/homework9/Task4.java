/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework9;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static int printTriangle(int startIndex, int n) {
        if (startIndex > n) {
            return -1;
        }
        print(1, startIndex);
        return printTriangle(startIndex + 1, n);
    }

    private static int print(int startIndex, int endIndex) {
        if (startIndex > endIndex) {
            System.out.println();
            return -1;
        }
        System.out.print(startIndex);
        return print(startIndex + 1, endIndex);
    }

    public static void main(String[] args) {
        printTriangle(1, 9);
    }
}
