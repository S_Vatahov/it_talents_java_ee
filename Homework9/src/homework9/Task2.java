/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework9;

/**
 *
 * @author slavi
 */
public class Task2 {

    private static int multiplication(int x, int y) {
        if (y == 1) {
            return x;
        }
        return x + multiplication(x, y - 1);
    }

    public static void main(String[] args) {
        System.out.println(multiplication(10, 5));
    }
}
