/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework9;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static int getReverseNumb(int n, int number) {
        int dig = n % 10;
        n /= 10;
        if (n == 0) {
            return number * 10 + dig;
        }
        return getReverseNumb(n, number * 10 + dig);
    }

    private static boolean isPalindrom(int num1, int num2) {
        int dig1 = num1 % 10;
        int dig2 = num2 % 10;

        if (dig1 != dig2) {
            return false;
        }
        if (num1 == 0) {
            return true;
        }
        return isPalindrom(num1 / 10, num2 / 10);
    }

    public static void main(String[] args) {
        int number = 123321;
        int revers = getReverseNumb(number, 0);
        System.out.println(revers);
        System.out.println(isPalindrom(number, revers));
    }
}
