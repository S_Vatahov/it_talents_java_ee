/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author slavi
 */
public final class Demo {

    private static final String[] NAMES = {"Ivan", "Dragan", "Petkan", "Ginka", "Penka", "Puhi", "Niki", "VR46", "MM93", "TC222", "JH84", "KR94"};
    private static final String[] CARMODELS = {"LADA", "MOSCVICH", "MERCEDES", "OPEL", "BMW", "TESLA", "AUDI", "SUBARU", "GOLF", "LAMBORGINI", "PORSHE"};
    private static final String[] BUSMODELS = {"MERCEDES", "SETRA", "IVECO", "Volkswagen", "MAN"};
    private static final String[] TRUCKMODELS = {"MERCEDES", "KAMAZ", "ZIL", "IVECO", "MAN", "TATRA"};

    private static void printPetrolStationVignettes(PetrolStation petrolStation) {
        Iterator<Vignette> itr = petrolStation.vignettes.iterator();
        while (itr.hasNext()) {
            Vignette vignette = itr.next();
            System.out.println("Price = " + vignette.getPrice() + " Color = " + vignette.getColor());
        }
    }

    private static HashSet<Driver> generateDrivers(int number, PetrolStation petrolStation) {
        HashSet<Driver> drivers = new HashSet(number);
        for (int i = 0; i < 20; i++) {
            boolean a = drivers.add(new Driver(NAMES[new Random().nextInt(NAMES.length)], new Random().nextInt(10000) + 1000, petrolStation));
            if (!a) {
                i--;
            }
        }
        return drivers;
    }

    private static ArrayList<Vehicle> generateVehicle(int number) {
        ArrayList<Vehicle> vehicles = new ArrayList<>(number);

        for (int i = 0; i < number; i++) {
            int r = new Random().nextInt(3);
            switch (r) {
                case 0:
                    vehicles.add(new Car(CARMODELS[new Random().nextInt(CARMODELS.length)], ThreadLocalRandom.current().nextInt(1900, 2017)));
                    break;
                case 1:
                    vehicles.add(new Bus(BUSMODELS[new Random().nextInt(BUSMODELS.length)], ThreadLocalRandom.current().nextInt(1900, 2017)));
                    break;
                case 2:
                    vehicles.add(new Truck(TRUCKMODELS[new Random().nextInt(TRUCKMODELS.length)], ThreadLocalRandom.current().nextInt(1900, 2017)));
                    break;
                default:
                    break;
            }
        }
        return vehicles;
    }

    private static void distributionOfVehicles(Set<Driver> drivers, ArrayList<Vehicle> vehicles) {
        Iterator<Driver> iterator = drivers.iterator();

        while (iterator.hasNext()) {
            int number = 10;
            Driver driver = iterator.next();
            do {
                driver.addVehicle(vehicles.remove(new Random().nextInt(vehicles.size())));
                number--;
            } while (number > 0);
        }

    }

    private static void buyVignette(Set<Driver> drivers) {

        Iterator<Driver> iterator = drivers.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Driver driver = iterator.next();
            i++;
            if (i == 3) {
                int r = new Random().nextInt(10);
                for (int index = 0; index < r; index++) {
                    driver.buyVignette(index);
                }
                i = 0;
                continue;
            }
            driver.buyVignette();
        }

    }

    public static void main(String[] args) {

        PetrolStation petrol = new PetrolStation();
        //printPetrolStationVignettes(petrol);
        HashSet<Driver> drivers = generateDrivers(20, petrol);
        ArrayList<Vehicle> vehicles = generateVehicle(200);
        distributionOfVehicles(drivers, vehicles);
        buyVignette(drivers);

    }
}
