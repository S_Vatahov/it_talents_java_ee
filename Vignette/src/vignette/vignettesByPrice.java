/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.Comparator;

/**
 *
 * @author slavi
 */
public class vignettesByPrice implements Comparator<Vignette> {

    @Override
    public int compare(Vignette v1, Vignette v2) {
        return Integer.valueOf(v1.getPrice()).compareTo(v2.getPrice());
    }
}
