/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

/**
 *
 * @author slavi
 */
public class TruckVignette extends Vignette {

    public TruckVignette(int multiplier, Period period) {
        super("Red", 9 * multiplier, period);
    }

    @Override
    int stick() {
        return 10;
    }

}
