/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author slavi
 */
public final class Driver {

    private String name;
    private int money;
    private final PetrolStation petrolStation;
    final List<Vehicle> vehicles;

    public Driver(String name, int money, PetrolStation petrolStation) {
        setName(name);
        setMoney(money);
        vehicles = new ArrayList<>();
        this.petrolStation = petrolStation;
    }

    String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.equals("")) {
            System.out.println("Invalid name");
        } else {
            this.name = name;
        }
    }

    int getMoney() {
        return money;
    }

    private void setMoney(int money) {
        this.money = money;
    }

    boolean addVehicle(Vehicle newVehicle) {
        return vehicles.add(newVehicle);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + this.money;
        hash = 59 * hash + Objects.hashCode(this.petrolStation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Driver other = (Driver) obj;
        if (this.money != other.money) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.petrolStation, other.petrolStation);
    }

    boolean buyVignette(int index) {
        if (vehicles.get(index) instanceof Car) {
            Vignette v = this.petrolStation.saleOfAVignette(VehicleType.CAR, Period.values()[new Random().nextInt(Period.values().length)]);
            this.money -= v.getPrice();
            return true;
        } else if (vehicles.get(index) instanceof Bus) {
            Vignette v = this.petrolStation.saleOfAVignette(VehicleType.BUS, Period.values()[new Random().nextInt(Period.values().length)]);
            this.money -= v.getPrice();
            return true;
        } else if (vehicles.get(index) instanceof Truck) {
            Vignette v = this.petrolStation.saleOfAVignette(VehicleType.TRUCK, Period.values()[new Random().nextInt(Period.values().length)]);
            this.money -= v.getPrice();
            return true;
        }

        return false;
    }

    void buyVignette() {

        vehicles.stream().map((v) -> {
            return v;
        }).forEachOrdered((v) -> {
            if (v instanceof Car) {
                Vignette vv = this.petrolStation.saleOfAVignette(VehicleType.CAR, Period.values()[new Random().nextInt(Period.values().length)]);
                this.money -= vv.getPrice();

            } else if (v instanceof Bus) {
                Vignette vv = this.petrolStation.saleOfAVignette(VehicleType.BUS, Period.values()[new Random().nextInt(Period.values().length)]);
                this.money -= vv.getPrice();

            } else if (v instanceof Truck) {
                Vignette vv = this.petrolStation.saleOfAVignette(VehicleType.TRUCK, Period.values()[new Random().nextInt(Period.values().length)]);
                this.money -= vv.getPrice();

            }
        });

    }

    int getNumberVehicle() {
        return vehicles.size();
    }
}
