/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.Calendar;

/**
 *
 * @author slavi
 */
public class Vehicle {

    private String model;
    private Vignette vignette;
    private int year;

    public Vehicle(String model, int year) {
        this.setModel(model);
        this.setYear(year);

    }

    String getModel() {
        return model;
    }

    final void setModel(String model) {
        if (model == null || model.equals("")) {
            System.err.println("Invalid model");
            return;
        }
        this.model = model;
    }

    Vignette getVignette() {
        return vignette;
    }

    final void setVignette(Vignette vignette) {
        if (vignette == null) {
            System.err.println("Invalid vignette");
            return;
        }
        this.vignette = vignette;
    }

    int getYear() {
        return year;
    }

    public final void setYear(int year) {
        Calendar now = Calendar.getInstance();
        int currYear = now.get(Calendar.YEAR);

        if (year < 1900 || year > currYear) {
            System.out.println("Invalid year");
        }
        this.year = year;
    }

}
