/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.Date;

/**
 *
 * @author slavi
 */
public abstract class Vignette {

    private Date date;
    private final String color;
    private final Period period;
    private final int price;

    public Vignette(String color, int price, Period period) {
        this.color = color;
        this.price = price;
        this.period = period;
    }

    int getPrice() {
        return price;
    }

    abstract int stick();

    public String getColor() {
        return color;
    }

    public Period getPeriod() {
        return period;
    }
}
