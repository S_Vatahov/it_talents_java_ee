/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

/**
 *
 * @author slavi
 */
public class BusVignette extends Vignette {

    public BusVignette(int multiplier, Period period) {
        super("Blue", 7 * multiplier, period);
    }

    @Override
    int stick() {
        return 20;
    }
}
