/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

/**
 *
 * @author slavi
 */
public class CarVignette extends Vignette {

    public CarVignette(int multiplier, Period period) {
        super("Green", 5 * multiplier, period);
    }

    @Override
    int stick() {
        return 5;
    }
}
