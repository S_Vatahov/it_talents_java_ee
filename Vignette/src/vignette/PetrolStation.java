/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vignette;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author slavi
 */
public class PetrolStation {

    double turnover;
    List<Vignette> vignettes;
    private final int NUMBERVIGNETTES = 10000;

    public PetrolStation() {
        vignettes = new ArrayList<>(NUMBERVIGNETTES);
        generateVignettes(NUMBERVIGNETTES);
    }

    private void generateVignettes(int number) {
        int multiplier;
        Period period = null;
        do {
            int VehilceType = new Random().nextInt(3);
            int periodFactor = new Random().nextInt(3);

            switch (periodFactor) {
                case 0:
                    multiplier = 1;
                    period = Period.ONE_DAY;
                    break;
                case 1:
                    multiplier = 10;
                    period = Period.ONE_MOUNT;
                    break;
                case 2:
                    multiplier = 60;
                    period = Period.ONE_YEAR;
                    break;
                default:
                    multiplier = 0;

                    break;

            }
            switch (VehilceType) {
                case 0:
                    vignettes.add(new CarVignette(multiplier, period));
                    break;
                case 1:
                    vignettes.add(new BusVignette(multiplier, period));
                    break;
                case 2:
                    vignettes.add(new TruckVignette(multiplier, period));
                    break;
            }
            number--;
        } while (number > 0);
        Collections.sort(vignettes, new vignettesByPrice());
    }

     Vignette saleOfAVignette(VehicleType vehicleType, Period period) {
        Vignette vignette = fintdVignetteType(vehicleType, period);
        int index = Collections.binarySearch(vignettes, vignette, new vignettesByPrice());
        return vignettes.remove(index);
    }

    private Vignette fintdVignetteType(VehicleType vehicle, Period period) {
        if (null != vehicle) {
            switch (vehicle) {
                case CAR:
                    if (null != period) {
                        switch (period) {
                            case ONE_DAY:
                                return new CarVignette(1, period);
                            case ONE_MOUNT:
                                return new CarVignette(10, period);
                            case ONE_YEAR:
                                return new CarVignette(60, period);
                            default:
                                break;
                        }
                    }
                    break;
                case BUS:
                    if (null != period) {
                        switch (period) {
                            case ONE_DAY:
                                return new BusVignette(1, period);
                            case ONE_MOUNT:
                                return new BusVignette(10, period);
                            case ONE_YEAR:
                                return new BusVignette(60, period);
                            default:
                                break;
                        }
                    }
                    break;
                case TRUCK:
                    if (null != period) {
                        switch (period) {
                            case ONE_DAY:
                                return new TruckVignette(1, period);
                            case ONE_MOUNT:
                                return new TruckVignette(10, period);
                            case ONE_YEAR:
                                return new TruckVignette(60, period);
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return null;
    }
}
