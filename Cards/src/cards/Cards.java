/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cards;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Cards {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> cards = new ArrayList();
        String[] colors = {"Pica", "Qupa", "Caro", "Spatia"};
        String[] card = {"2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "10 ", "11 J", "12 Q", "13 K", "14 A"};

        for (String card1 : card) {
            for (String color : colors) {
                cards.add(card1.concat(color));
            }
        }
        System.out.println(cards.toString());
        Scanner input = new Scanner(System.in);
        int card1Index;
        int card2Index;
        int card3Index;

        while (true) {
            do {
                System.out.println("Enter card1 [0-51]");
                card1Index = input.nextInt();
            } while (card1Index < 0 || card1Index > 51);
            do {
                System.out.println("Enter card2 [0-51]");
                card2Index = input.nextInt();
            } while (card2Index < 0 || card2Index > 51);
            do {
                System.out.println("Enter card3 [0-51]");
                card3Index = input.nextInt();
            } while (card3Index < 0 || card3Index > 51);

            if (Integer.parseInt(cards.get(card1Index).substring(0, 2).trim()) < Integer.parseInt(cards.get(card2Index).substring(0, 2).trim())
                    && Integer.parseInt(cards.get(card2Index).substring(0, 2).trim()) < Integer.parseInt(cards.get(card3Index).substring(0, 2).trim())) {
                break;
            }

        }
    }

}
