/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static void printRatio(int[][] arr) {
        int currRowSum = 0;
        int maxRowSum = 0;
        int currColSum = 0;
        int maxColSum = 0;
        // The solution is only for a square matrix !!!
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                currRowSum += arr[i][j];
                currColSum += arr[j][i];
            }
            if (maxRowSum < currRowSum) {
                maxRowSum = currRowSum;
            }
            if (currColSum > maxColSum) {
                maxColSum = currColSum;
            }
            currColSum = 0;
            currRowSum = 0;
        }
        System.out.println("Max row sum is " + maxRowSum);
        System.out.println("Max col sum is " + maxColSum);

        if (maxRowSum > maxColSum) {
            System.out.println("Max row sum > max col sum");
        } else {
            System.out.println("Max row sum < max col sum");
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 16}
        };
        printRatio(arr);
    }
}
