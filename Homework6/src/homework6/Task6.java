/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task6 {

    private static void printSum(int[][] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                continue;
            }
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + (j == arr.length - 1 ? " = " : "+"));
                sum += arr[i][j];
            }
            System.out.println(sum);
            sum = 0;
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {11, 12, 13, 14, 15, 16},
            {21, 22, 23, 24, 25, 26},
            {31, 32, 33, 34, 35, 36},
            {41, 42, 43, 44, 45, 46},
            {51, 52, 53, 54, 55, 56},
            {61, 62, 63, 64, 65, 66}
        };
        printSum(arr);
    }
}
