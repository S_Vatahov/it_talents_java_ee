/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static void printSumAndAvg(int[][] arr) {
        int sum = 0;
        int counter = 0;
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                sum += arr1[j];
                counter++;
            }
        }
        int avg = sum / counter;
        System.out.println("The sum is " + sum);
        System.out.println("The avg is " + avg);
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 16},};
        printSumAndAvg(arr);
    }
}
