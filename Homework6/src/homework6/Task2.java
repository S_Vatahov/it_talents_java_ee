/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task2 {

    static Scanner input = new Scanner(System.in);

    private static void fillArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print("Enter array[" + i + "][" + j + "] = ");
                arr[i][j] = input.nextInt();

            }

        }
    }

    private static void printDiagonals(int[][] arr) {
        System.out.println("First   |   Second");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][i] + "       |   " + arr[i][arr.length - i - 1]);
        }
    }

    public static void main(String[] args) {

        int n;
        do {
            System.out.print("Enter array length = ");
            n = input.nextInt();
        } while (n < 0);
        int[][] array = new int[n][n];
        fillArray(array);
        printDiagonals(array);

    }
}
