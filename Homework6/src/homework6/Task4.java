/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static void printArr(int[][] arr) {
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    private static int[][] rotateTo90Degree(int[][] arr) {
        int[][] helpArr = new int[arr.length][arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j < arr.length; j++) {
                helpArr[j][arr.length - i - 1] = arr[i][j];
            }
        }
        return helpArr;
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 16}
        };
        arr = rotateTo90Degree(arr);
        printArr(arr);
    }
}
