/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static void printSum(int[][] arr) {

        int totlSum = 0;
        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            if (i % 2 == 0) {
                System.out.print(arr[i][0] + "+" + arr[i][2] + "+" + arr[i][4] + " = ");
                sum = arr[i][0] + arr[i][2] + arr[i][4];
                totlSum += arr[i][0] + arr[i][2] + arr[i][4];
            } else {
                System.out.print(arr[i][1] + "+" + arr[i][3] + "+" + arr[i][5] + " = ");
                sum = arr[i][1] + arr[i][3] + arr[i][5];
                totlSum += arr[i][1] + arr[i][3] + arr[i][5];
            }
            System.out.println(sum);
        }
        System.out.println("Total sum is " + totlSum);
    }

    public static void main(String[] args) {
        int[][] arr = {
            {11, 12, 13, 14, 15, 16},
            {21, 22, 23, 24, 25, 26},
            {31, 32, 33, 34, 35, 36},
            {41, 42, 43, 44, 45, 46},
            {51, 52, 53, 54, 55, 56},
            {61, 62, 63, 64, 65, 66}
        };
        printSum(arr);
    }
}
