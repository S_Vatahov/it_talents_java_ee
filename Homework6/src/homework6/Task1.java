/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static int minNum(int[][] arr) {

        int min = arr[0][0];
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                if (arr1[j] < min) {
                    min = arr1[j];
                }
            }
        }
        return min;
    }

    private static int maxNum(int[][] arr) {

        int max = arr[0][0];
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                if (arr1[j] > max) {
                    max = arr1[j];
                }
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[][] arr = {
            {48, 72, 13, 14, 15},
            {21, 22, 53, 24, 75},
            {31, 57, 33, 34, 35},
            {41, 95, 43, 44, 45},
            {59, 52, 53, 54, 55},
            {61, 69, 63, 64, 65}
        };
        System.out.println("The min number is " + minNum(arr));
        System.out.println("The max number is " + maxNum(arr));
    }

}
