/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static void ratio(String s1) {
        int upperCase = 0;
        int lowerCase = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (Character.isUpperCase(s1.charAt(i))) {
                upperCase++;
            } else {
                lowerCase++;
            }
        }
        for (int i = 2; i <= 7; i++) {
            if (upperCase % i == 0 && lowerCase % i == 0) {
                upperCase /= i;
                lowerCase /= i;
                i = 2;
            }
        }
        System.out.println(lowerCase + ":" + upperCase);

    }

    public static void main(String[] args) {
        String s1 = "aaaaAasdfasdcfAaSdasfAdasfasddfasdfascfasfASSsAAAAAAAAAAA";
        ratio(s1);
    }
}
