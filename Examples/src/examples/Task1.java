/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static int pow(int x, int y) {
        if (y == 1) {
            return x;
        }
         return sumPow(pow(x, y-1), x);
    }

    private static int sumPow(int x, int y) {
        if (y == 1) {
            return x;
        }
        return x + sumPow(x, y - 1);
    }

    public static void main(String[] args) {
            System.out.println(pow(2, 8));
    }

}
