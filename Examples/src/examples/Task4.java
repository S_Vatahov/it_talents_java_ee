/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examples;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static void printNegativNumbs(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0) {
                break;
            }
            System.out.print(arr[i] + "|");
        }
    }

    public static void main(String[] args) {
        int[] arr = {-50, -49, -25, -13, -10, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        printNegativNumbs(arr);
    }
}
