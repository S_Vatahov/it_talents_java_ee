/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Demo {
    
    private static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(ClassLoader.getSystemResource(file).getFile()));
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
    
    public static void main(String[] args) throws IOException {
          
        ArrayList<Page> pages = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            pages.add(new Page("Page " + (i + 1)));
        }
        // Ако не се сетне парола работи като  SimpleNotepad
        // Пример за валидна парола -  bbAHK8k@
        SecuredNotepad secureNotepad = new SecuredNotepad(pages);
        
        secureNotepad.setPassword();
        
        String page1 = readFile("res/page1.txt");
        secureNotepad.add(page1, 0);
        
       for (int i = 0; i < pages.size(); i++) {
           String a = readFile("res/page" + (i + 1) + ".txt");
           secureNotepad.add(a, i);
       }
        secureNotepad.viewPages();
    }
}
