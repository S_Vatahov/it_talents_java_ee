/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework20;

/**
 *
 * @author slavi
 */
public class Page {

    private final String title;
    private final StringBuilder textBuild;
    static final String ANSI_GREEN = "\u001B[32m";
    static final String ANSI_BLACK = "\u001B[30m";

    Page(String title) {
        this.title = title;
        this.textBuild = new StringBuilder();
    }

    void addText(String text) {
        this.textBuild.append(text);
        this.textBuild.append("\n");
    }

    void cleanPage() {
        this.textBuild.delete(0, this.textBuild.length());
    }

    String browsePage() {
        String page = new String();
        page = page.concat(Page.ANSI_GREEN + "              " + title + Page.ANSI_BLACK + "\n\n").concat(this.textBuild.toString());

        return page;
    }
}
