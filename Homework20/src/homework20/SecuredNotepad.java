/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework20;

import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

/**
 *
 * @author slavi
 */
public class SecuredNotepad implements INotepad {

    private String myPassword;
    private final ArrayList<Page> pages;

    public SecuredNotepad(ArrayList<Page> pages) {
        this.pages = pages;
    }

    @Override
    public void add(String text, int n) {
        if (!checkingPassword()) {
            return;
        }
        if (!validate(text, n)) {
            return;
        }
        this.pages.get(n).addText(text);

    }

    @Override
    public void put(String text, int n) {
        if (!checkingPassword()) {
            return;
        }

        if (!validate(text, n)) {
            return;
        }
        this.pages.get(n).cleanPage();
        this.pages.get(n).addText(text);

    }

    @Override
    public void delete(int n) {
        if (!checkingPassword()) {
            return;
        }
        if (n < 0 || n > this.pages.size()) {
            System.err.println("Invalid page number");
            return;
        }
        this.pages.get(n).cleanPage();

    }

    @Override
    public void viewPages() {
        if (!checkingPassword()) {
            return;
        }
        for (Page page : this.pages) {
            System.out.println(page.browsePage());
        }

    }

    private boolean checkingPassword() {
        if (myPassword == null) {
            return true;
        }
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Enter a password:");
        JPasswordField passwordField = new JPasswordField(10);
        panel.add(label);
        panel.add(passwordField);
        String[] options = new String[]{"OK", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, panel, "Validation",
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[1]);

        if (option == 0) {
            String password = charArrToString(passwordField.getPassword());
            if (this.myPassword.equals(password)) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid password");
                return false;
            }
        }
        return false;
    }

    private String charArrToString(char[] pass) {
        String password = new String();
        for (int i = 0; i < pass.length; i++) {
            password = password.concat(Character.toString(pass[i]));
        }
        return password;
    }

    void setPassword() {
        if (this.myPassword != null) {
            if (!checkingPassword()) {
                System.err.println("Invalid password");
                return;
            }
        }

        JPanel panel = new JPanel();
        JLabel label = new JLabel("Enter  password:");
        JPasswordField passwordField = new JPasswordField(10);
        panel.add(label);
        panel.add(passwordField);
        String[] options = new String[]{"OK", "Cancel"};
        int option = JOptionPane.showOptionDialog(null, panel, "Set Password",
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[1]);

        if (option == 0) {
            String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
            String password = charArrToString(passwordField.getPassword());
            if (!password.matches(pattern)) {
                JOptionPane.showMessageDialog(null, "Enter a secure password!");
                setPassword();
                return;
            }
            this.myPassword = password;
        }
    }

    private boolean validate(String text, int n) {
        if (n < 0 || n > this.pages.size()) {
            System.err.println("Invalid page number");
            return false;
        }
        if (text == null) {
            System.out.println("Invalid text");
            return false;
        }
        return true;
    }
}
