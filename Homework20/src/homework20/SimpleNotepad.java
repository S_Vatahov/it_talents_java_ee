/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework20;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class SimpleNotepad implements INotepad {

    private final ArrayList<Page> pages;

    public SimpleNotepad(ArrayList<Page> pages) {
        this.pages = pages;
    }

    @Override
    public void add(String text, int n) {
        if (!validate(text, n)) {
            return;
        }
        this.pages.get(n).addText(text);

    }

    @Override
    public void put(String text, int n) {
        if (!validate(text, n)) {
            return;
        }
        this.pages.get(n).cleanPage();
        this.pages.get(n).addText(text);
    }

    @Override
    public void delete(int n) {
        if (n < 0 || n > this.pages.size()) {
            System.err.println("Invalid page number");
            return;
        }
        this.pages.get(n).cleanPage();
    }

    @Override
    public void viewPages() {

        for (Page page : this.pages) {
            page.browsePage();
        }
    }

    private boolean validate(String text, int n) {
        if (n < 0 || n > this.pages.size()) {
            System.err.println("Invalid page number");
            return false;
        }
        if (text == null) {
            System.out.println("Invalid text");
            return false;
        }
        return true;
    }

}
