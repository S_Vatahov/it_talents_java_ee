/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework20;

/**
 *
 * @author slavi
 */
public interface INotepad {

    void add(String text, int n);

    void put(String text, int n);

    void delete(int n);

    void viewPages();
    
}
