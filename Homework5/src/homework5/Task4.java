/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static boolean isMirrored(int[] arr, int index) {
        if (index == arr.length - 1) {
            return true;
        }
        if (arr[index] == arr[arr.length - index - 1]) {
            return isMirrored(arr, index + 1);
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        int[] arr = {3, 7, 7, 3};
        System.out.println(isMirrored(arr, 0));
    }
}
