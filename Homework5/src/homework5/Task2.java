/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task2 {

    private static int fillArray(int[] arr1, int[] arr2, int index1, int index2) {
        if (index1 == arr1.length) {
            arr2[arr2.length - 1] = arr1[0];
            return arr1[index1 - 1];

        }
        arr2[index1] = arr1[index1];
        index2 -= 1;
        arr2[index2] = fillArray(arr1, arr2, index1 + 1, index2);
        return arr1[index1];
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] arr2 = new int[arr.length * 2];
        fillArray(arr, arr2, 0, arr2.length - 1);

        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + "|");
        }
    }
}
