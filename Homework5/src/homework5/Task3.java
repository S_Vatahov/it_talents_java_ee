/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static int fillArray(int[] arr, int index, int n) {
        if (index == 0 || index == 1) {
            arr[index] = n;
            return n;
        }
        arr[index] = fillArray(arr, index - 1, n) + fillArray(arr, index - 2, n);
        return arr[index];
    }

    public static void main(String[] args) {
        int[] arr = new int[10];
        int n = 1;
        fillArray(arr, arr.length - 1, n);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }

    }
}
