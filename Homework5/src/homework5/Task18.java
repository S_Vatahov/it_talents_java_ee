/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task18 {

    private static int fillArray(int[] arr1, int[] arr2, int[] arr3, int index) {
        if (index == arr1.length) {
            return -1;
        }
        if (arr1[index] > arr2[index]) {
            arr3[index] = arr1[index];
        } else {
            arr3[index] = arr2[index];
        }
        return fillArray(arr1, arr2, arr3, index + 1);
    }

    public static void main(String[] args) {
        int[] arr1 = {18, 19, 32, 1, 3, 4, 5, 6, 7, 8};
        int[] arr2 = {1, 2, 3, 4, 5, 16, 17, 18, 27, 11};
        int[] arr3 = new int[arr1.length];
        fillArray(arr1, arr2, arr3, 0);

        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i] + "|");
        }
    }
}
