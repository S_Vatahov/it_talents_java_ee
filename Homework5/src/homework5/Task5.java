/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static int fillArray(int[] arr, int index) {
        if (index == arr.length - 1) {
            return index * 3;
        }
        arr[index + 1] = fillArray(arr, index + 1);
        return index * 3;
    }

    public static void main(String[] args) {
        int[] arr = new int[10];
        fillArray(arr, 0);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }
    }
}
