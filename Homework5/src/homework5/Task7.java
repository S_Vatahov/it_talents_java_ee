/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static int fillArray(int[] arr1, int[] arr2, int index) {
        if (index == arr1.length - 1) {
            arr2[index] = arr1[index];
            return arr1[index];
        }
        if (index == 0) {
            arr2[index] = arr1[index];
        } else {
            arr2[index]= arr1[index-1]+arr1[index+1];
        }
        return fillArray(arr1, arr2, index+1);
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] arr2 = new int[arr1.length];
        fillArray(arr1, arr2, 0);
        
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + "|");
        }
    }
}
