/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task11 {

    private static int printNumbDivByFive(int[] arr, int index) {
        if (index == arr.length - 1) {
            return -1;
        }
        if (arr[index] % 5 == 0 && arr[index] > 5) {
            System.out.println(arr[index]);
        }
        return printNumbDivByFive(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {-23, -55, 17, 75, 56, 105, 134};
        printNumbDivByFive(arr, 0);
    }
}
