/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task14 {

    private static int fillArray(double[] arr1, double[] arr2, int index, int index2) {
        if (index == arr1.length) {
            return -1;
        }
        if (arr1[index] > -2.99 && arr1[index] < 2.99) {
            arr2[index2++] = arr1[index];
        }
        return fillArray(arr1, arr2, index + 1, index2);
    }

    private static int getLength(double[] arr, int index, int count) {
        if (index == arr.length) {
            return count;
        }
        if (arr[index] > -2.99 && arr[index] < 2.99) {
            count++;
        }
        return getLength(arr, index + 1, count);
    }

    public static void main(String[] args) {
        double[] arr = {7.1, 8.5, 0.2, 3.7, 0.99, 1.4, -3.5, -110, 212, 341, 1.2};

        double[] arr2 = new double[getLength(arr, 0, 0)];
        fillArray(arr, arr2, 0, 0);

        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + "|");
        }
    }
}
