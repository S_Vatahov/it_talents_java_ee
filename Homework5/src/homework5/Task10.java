/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static int avg(int[] arr, int index, int sum) {
        if (index == arr.length) {
            return sum / arr.length;
        }
        sum += arr[index];
        return avg(arr, index + 1, sum);
    }

    private static int avgNumb(int[] arr, int index, int avg) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int num = avgNumb(arr, index + 1, avg);
        if (Math.abs(avg - num) < Math.abs(avg - arr[index])) {
            return num;
        } else {
            return arr[index];
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 15};

        System.out.println(avgNumb(arr, 0, avg(arr, 0, 0)));
    }
}
