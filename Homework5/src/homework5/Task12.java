/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task12 {

    private static int swap(int[] arr, int index) {
        if (index == arr.length - 1) {
            return -1;
        }
        switch (index) {
            case 0:
                int temp = arr[index];
                arr[index] = arr[index + 1];
                arr[index + 1] = temp;
                break;
            case 2:
                arr[index] += arr[index + 1];
                arr[index + 1] = arr[index] - arr[index + 1];
                arr[index] = arr[index] - arr[index + 1];
                break;
            case 4:
                arr[index] *= arr[index + 1];
                arr[index + 1] = arr[index] / arr[index + 1];
                arr[index] = arr[index] / arr[index + 1];
                break;
            default:
                break;
        }
        return swap(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        swap(arr, 0);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }
    }
}
