/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task9 {

    private static int reverseArr(int[] arr, int index, int index2) {
        if (index == arr.length - 1) {
            return index;
        }
        int tem = arr[index2];
        arr[index2] = reverseArr(arr, index + 1, index2 - 1);
        arr[index] = tem;
        return index;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        reverseArr(arr, 0, arr.length - 1);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }
    }
}
