/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task15 {

    private static void bubbleSort(double [] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    double temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        double[] arr = {7.13, 0.2, 4.9, 5.1, 6.34, 1.12};
        bubbleSort(arr);
        
        System.out.println(arr[arr.length-3]);
        System.out.println(arr[arr.length-2]);
        System.out.println(arr[arr.length-1]);
    }
}
