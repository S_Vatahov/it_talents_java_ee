/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static int lowestNumberDevByThree(int[] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int minDividedNumb = lowestNumberDevByThree(arr, index + 1);
        if (minDividedNumb < arr[index] && minDividedNumb % 3 == 0) {
            return minDividedNumb;
        } else if (arr[index] % 3 == 0) {
            return arr[index];
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {-6, -30, 5, 8, 6, -3, -7};
        int num = lowestNumberDevByThree(arr, 0);
        if (num == -1) {
            System.out.println("There is no number in the array divided by 3");
        } else {
            System.out.println("The lowest number being divided into three is " + num);
        }
    }

}
