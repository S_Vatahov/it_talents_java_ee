/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task8 {

    private static int mostLargestSeries(int[] arr, int index, int currCount, int maxCount, int element) {
        if (index == arr.length) {
            System.out.println("The most largest series is " + element + " " + maxCount + " times");
            return -1;
        }
        if (index > 0 && arr[index] == arr[index - 1]) {
            currCount++;
        }
        if ((index > 0 && arr[index] != arr[index - 1]) || index == arr.length - 1) {
            if (maxCount < currCount) {
                maxCount = currCount;
                element = arr[index - 1];
            }
            currCount = 1;
        }
        return mostLargestSeries(arr, index + 1, currCount, maxCount, element);
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 3, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 8, 8, 8, 8};
        mostLargestSeries(arr, 0, 1, 0, 0);
    }
}
