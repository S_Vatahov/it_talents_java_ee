/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task16 {

    private static int print(double[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        System.out.printf("%.2f", arr[index]);
        System.out.print("|");
        return print(arr, index + 1);
    }

    private static int changeArray(double[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        if (arr[index] < -0.231) {
            arr[index] = ((index+1) * (index+1)) + 41.25;
        } else {
            arr[index] = arr[index] * (index+1);
        }
        return changeArray(arr, index + 1);
    }

    public static void main(String[] args) {
        double[] arr = {-1.12, -2.43, 3.1, 4.2, 0, 6.4, -7.5, 8.6, 9.1, -4};
        print(arr, 0);
        changeArray(arr, 0);
        System.out.println();
        print(arr, 0);
    }
}
