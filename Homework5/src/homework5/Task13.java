/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework5;

/**
 *
 * @author slavi
 */
public class Task13 {

    private static int fillArray(int[] arr, int index, int n) {
        if (n == 0) {
            return -1;
        }
        if (n < 0) {
            arr[0] = 1;
            n *= -1;
        }
        int dig = n % 2;
        n /= 2;
        arr[index] = dig;
        return fillArray(arr, index - 1, n);
    }

    public static void main(String[] args) {
        int n = 99;
        int[] arr = new int[32];
        fillArray(arr, arr.length - 1, n);

        boolean isZerro = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                isZerro = false;
            }
            if (isZerro) {
                continue;
            }
            System.out.print(arr[i] + "|");
        }
    }
}
