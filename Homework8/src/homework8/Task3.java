/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static void equalsByPosition(String s1, String s2) {
        int length;
        if (s1.length() <= s2.length()) {
            System.out.println("S1 length <= S2 length");
            length = s1.length();
        } else {
            System.out.println("S1 length > S2 length");
            length = s1.length();
        }
        for (int i = 0; i < length; i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                System.out.println((i + 1) + " " + s1.charAt(i) + "-" + s2.charAt(i));
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1, s2;
        do {
            System.out.println("Enter s1");
            s1 = input.next();
        } while (s1.length() > 40);
        do {
            System.out.println("Enter s2");
            s2 = input.next();
        } while (s2.length() > 40);
        equalsByPosition(s1, s2);
    }
}
