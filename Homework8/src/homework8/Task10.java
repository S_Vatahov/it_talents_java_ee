/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static String changeString(String s1) {
        String s2 = "";
        for (int i = 0; i < s1.length(); i++) {
            s2 += (Character.toString((char) ((int) s1.charAt(i) + 5)));
        }
        return s2;
    }

    public static void main(String[] args) {
        String s1 = "Hello";
        System.out.println(changeString(s1));
    }
}
