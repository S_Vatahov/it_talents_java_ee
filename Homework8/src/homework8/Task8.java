/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task8 {

    private static boolean isPalindrom(String s, int index) {
        if (index == s.length() / 2 - 1) {
            return true;
        }
        if (s.charAt(index) != s.charAt(s.length() - index - 1)) {
            return false;
        }
        return isPalindrom(s, index + 1);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter text");
        String s1 = input.nextLine();
        System.out.println(isPalindrom(s1, 0));
    }
}
