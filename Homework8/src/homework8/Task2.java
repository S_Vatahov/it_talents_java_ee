/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1, s2;
        do {
            System.out.println("Enter s1");
            s1 = input.next();
        } while (s1.length() > 20 || s1.length() < 10);
        do {
            System.out.println("Enter s2");
            s2 = input.next();
        } while (s2.length() > 20 || s2.length() < 10);
        String s3 = s1;
        s1 = s2.substring(0, 5).concat(s1.substring(5));
        s2 = s3.substring(0, 5).concat(s2.substring(5));

        System.out.println("S1 = " + s1);
        System.out.println("S2 = " + s2);

        System.out.println(s1.length() > s2.length() ? "S1 length = " + s1.length() : "S2 lenght = " + s2.length());

    }
}
