/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static void getCrossesIndex(String s1, String s2) {
        int indexS1 = 0;
        int indexS2 = 0;
        boolean flag = false;
        for (int i = 0; i < s1.length(); i++) {
            if (flag) {
                break;
            }
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    indexS1 = i + 1;
                    indexS2 = j + 1;
                    flag = true;
                    break;
                }
            }
        }
        print(s1, s2, indexS1, indexS2);
    }

    private static void print(String s1, String s2, int indexS1, int indexS2) {

        for (int i = 0; i < s1.length(); i++) {
            int curIndex = indexS2;
            if (i + 1 == indexS1) {
                System.out.println(s2);
            } else {
                while (curIndex > 1) {
                    System.out.print(" ");
                    curIndex--;
                }
                System.out.println(s1.charAt(i));
            }
        }
    }

    public static void main(String[] args) {
        String s1 = "Sudoku";
        String s2 = "MineSwaper";
        getCrossesIndex(s1, s2);
    }
}
