/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task6 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter text");
        String s1 = input.nextLine();

        Scanner sc = new Scanner(s1);
        while (sc.hasNext()) {
            String s = sc.next();
            System.out.print(s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
        }

    }
}
