/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1, s2;
        do {
            System.out.println("Enter s1");
            s1 = input.next();
        } while (s1.length() > 40);
        do {
            System.out.println("Enter s2");
            s2 = input.next();
        } while (s2.length() > 40);
        
        System.out.println(s1.toLowerCase());
        System.out.println(s1.toUpperCase());
        System.out.println(s2.toLowerCase());
        System.out.println(s2.toUpperCase());
    }

}
