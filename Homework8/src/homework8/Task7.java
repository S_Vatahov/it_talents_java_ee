/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task7 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter text");
        String s1 = input.nextLine();
        String[] ss = s1.split(" ");
        System.out.println("The number of words is " + ss.length);
        int length = 0;
        for (int i = 0; i < ss.length; i++) {
            if (ss[i].length() > length) {
                length = ss[i].length();
            }
        }
        System.out.println("The longest word is there " + length + " characters");
    }
}
