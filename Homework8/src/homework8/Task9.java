/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework8;

/**
 *
 * @author slavi
 */
public class Task9 {
    
    private static int sum(String[] s1, int index) {
        if (index == s1.length - 1) {
            return Integer.parseInt(s1[index]);
        }
        return Integer.parseInt(s1[index]) + sum(s1, index + 1);
    }
    
    public static void main(String[] args) {
        String s1 = "asd-12sdf45-56asdf100";
        String s2 = s1.replaceAll("[^0-9-]", " ");
        s2 = s2.replaceAll("\\s+", " ");
        s2 = s2.replaceAll("-", " -").trim();
        String[] s3 = s2.split(" ");
        System.out.println("The sum is " + sum(s3, 0));
    }
}
