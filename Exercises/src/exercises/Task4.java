/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static int numberOfSentences(String s1) {
        String[] s2 = s1.split("\\.");
        return s2.length;
    }

    public static void main(String[] args) {
        String s1 = "Today is a good day for test. Sun is shining. The students are happy. The birds are\n"
                + "blue.";
        System.out.println(numberOfSentences(s1));
    }
}
