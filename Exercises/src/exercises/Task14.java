/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task14 {

    private static boolean isPositive(int[] arr, int index) {
        if (index == arr.length) {
            return true;
        }
        if (arr[index] < 0) {
            return false;
        }
        return isPositive(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(isPositive(arr, 0));
    }
}
