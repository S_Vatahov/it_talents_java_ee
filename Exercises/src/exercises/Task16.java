/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task16 {

    static int counter = 0;

    private static int reachKing(char[][] arr, int row, int col) {
        if (row < 0 || row >= arr.length || col < 0 || col >= arr.length) {
            return -1;
        }
        if (arr[row][col] == 'K') {
            counter++;
        }
        if (arr[row][col] != ' ') {
            return -1;
        }
        arr[row][col] = 'H';

        reachKing(arr, row + 1, col + 3);
        reachKing(arr, row - 1, col + 3);
        reachKing(arr, row + 3, col + 1);
        reachKing(arr, row - 1, col - 3);
        reachKing(arr, row + 1, col - 3);
        reachKing(arr, row - 3, col - 1);
        reachKing(arr, row + 3, col - 1);
        reachKing(arr, row + 3, col + 1);
        reachKing(arr, row - 3, col + 1);

        return counter;

    }

    private static void print(char[][] arr) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        char[][] arr = {
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', 'K', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},};
        System.out.println(reachKing(arr, 7, 1));
        print(arr);
    }
}
