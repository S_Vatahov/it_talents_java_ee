/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static String mostFrequent(String s1) {
        String[] s2 = s1.split(" ");
        String word = "";
        int currCounter = 0;
        int maxCounter = 0;

        for (int i = 0; i < s2.length; i++) {
            for (int j = i; j < s2.length; j++) {
                if (s2[j].equals(s2[i])) {
                    currCounter++;
                }
            }
            if (currCounter > maxCounter) {
                maxCounter = currCounter;
                word = s2[i];
            }
        }
        return word;
    }

    public static void main(String[] args) {
        String s = "11  11 2 2 3 4 5 6 11";
        System.out.println(mostFrequent(s));
    }
}
