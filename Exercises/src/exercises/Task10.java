/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static int numberOfNegative(int[] arr) {
        int start = 0;
        int end = arr.length;
        int mid = 0;
        while (true) {
            mid = (start + end) / 2;
            if (arr[mid] >= 0 && arr[mid - 1] < 0) {
                break;
            }
            if (arr[mid] > 0 && arr[mid - 1] >= 0) {
                end = mid - 1;
                if (end <= 1) {
                    mid = 0;
                    break;
                }
            } else {
                start = mid + 1;
            }
        }
        return mid;
    }

    public static void main(String[] args) {
        int[] arr = {-1,-2, 1, 2, 3, 4};
        System.out.println(numberOfNegative(arr));
    }
}
