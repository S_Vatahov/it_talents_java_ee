/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task18 {

    private static boolean isThereAway(char[][] arr, int rowS, int colS, int rowE, int colE, int counter) {
        if (rowS < 0 || rowS >= arr.length || colS < 0 || colS >= arr[0].length) {
            return false;
        }
        if (rowS == rowE && colS == colE) {
            return true;
        }
        if (arr[rowS][colS] != ' ') {
            return false;
        }

        arr[rowS][colS] = Integer.toString(counter).charAt(0);
        counter++;
        return isThereAway(arr, rowS + 1, colS, rowE, colE, counter)
                || isThereAway(arr, rowS - 1, colS, rowE, colE, counter)
                || isThereAway(arr, rowS, colS + 1, rowE, colE, counter)
                || isThereAway(arr, rowS, colS - 1, rowE, colE, counter);
    }

    private static void print(char[][] arr) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        char[][] lab = {
            {' ', '*', ' ', ' ', ' ', ' ', ' '},
            {' ', '*', ' ', '*', '*', '*', ' '},
            {' ', '*', ' ', ' ', ' ', '*', ' '},
            {' ', '*', '*', '*', ' ', '*', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', 'e'}
        };
        System.out.println(isThereAway(lab, 0, 0, 4, 6, 0));
        print(lab);

    }

}
