/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task1 {

    /**
     * @param args the command line arguments
     */
    private static void freeArea(char[][] arr, int row, int col) {
        if (row < 0 || row >= arr.length || col < 0 || col >= arr[row].length) {
            return;
        }
        if (arr[row][col] != ' ') {
            return;
        }
        arr[row][col] = '#';

        freeArea(arr, row - 1, col);
        freeArea(arr, row + 1, col);
        freeArea(arr, row, col - 1);
        freeArea(arr, row, col + 1);

    }

    private static void print(char[][] arr) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        char[][] arr = {
            {' ', ' ', ' ', ' '},
            {'b', 'b', 'b', ' '},
            {' ', ' ', ' ', ' '},
            {' ', 'b', 'b', 'b'},
            {' ', ' ', 'b', ' '},
            {'b', ' ', ' ', ' '},};
        freeArea(arr, 0, 0);
        print(arr);

    }
}
