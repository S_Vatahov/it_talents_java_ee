/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task9 {

    private static int differenceBetwinMaxMin(int arr[], int index, int max, int min) {
        if (index == arr.length) {
            return max - min;
        }
        if (arr[index] > max) {
            max = arr[index];
        }
        if (arr[index] < min) {
            min = arr[index];
        }
        return differenceBetwinMaxMin(arr, index + 1, max, min);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(differenceBetwinMaxMin(arr, 0, Integer.MIN_VALUE, Integer.MAX_VALUE));
    }
}
