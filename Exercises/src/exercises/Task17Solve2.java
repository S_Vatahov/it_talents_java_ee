package exercises;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Task17Solve2 {

    private static int mazze(char[][] arr, int row, int col, int counter) {
        if (row < 0 || row >= arr.length || col < 0 || col >= arr[row].length) {
            return 0;
        }
        if (arr[row][col] == 'e') {
            return counter;
        }
        if (arr[row][col] != ' ') {
            return 0;
        }
        counter++;
        arr[row][col] = 'S';

        int steps = mazze(arr, row + 1, col, counter)
                + mazze(arr, row - 1, col, counter)
                + mazze(arr, row, col + 1, counter)
                + mazze(arr, row, col - 1, counter);

        return steps;
    }

    public static void main(String[] args) {
        char[][] lab = {
            {' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', '*', ' ', '*', '*', '*', ' '},
            {' ', '*', ' ', ' ', ' ', '*', ' '},
            {' ', '*', '*', '*', ' ', '*', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', 'e'}
        };
        System.out.println("The shortes path is " + mazze(lab, 0, 0, 0));

    }

}
