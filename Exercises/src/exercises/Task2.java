/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

import java.util.ArrayList;

public class Task2 {

    private static void findPath(char[][] lab, int row, int col, ArrayList<String> allRoads, String path, char direction) {
        if ((col < 0) || (row < 0) || (col >= lab[0].length) || (row >= lab.length)) {
            return;
        }

        if (lab[row][col] == 'e') {
            allRoads.add(path);
        }
        if (lab[row][col] != ' ') {
            return;

        }

        path = path.concat(Character.toString(direction));
        lab[row][col] = 'S';

        findPath(lab, row, col - 1, allRoads, path, 'L');
        findPath(lab, row, col + 1, allRoads, path, 'R');
        findPath(lab, row - 1, col, allRoads, path, 'U');
        findPath(lab, row + 1, col, allRoads, path, 'D');

        lab[row][col] = ' ';

    }

    private static void print(char[][] arr, int minCounter) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
        System.out.println(minCounter);
    }

    public static void main(String[] args) {
        char[][] lab = {
            {' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', '*', '*', '*', '*', '*', ' '},
            {' ', '*', ' ', ' ', ' ', '*', ' '},
            {' ', '*', ' ', '*', ' ', '*', ' '},
            {' ', ' ', ' ', '*', ' ', ' ', 'e'}
        };
        ArrayList<String> path = new ArrayList<>();
        findPath(lab, 0, 0, path, "", '\u0000');
        System.out.println(path.toString());
    }
}
