/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task15 {

    private static boolean reachKing(char[][] arr, int row, int col) {
        if (row < 0 || row >= arr.length || col < 0 || col >= arr.length) {
            return false;
        }
        if (arr[row][col] == 'K') {
            return true;
        }
        if (arr[row][col] != ' ') {
            return false;
        }
        arr[row][col] = 'H';

        return reachKing(arr, row + 1, col + 2)
                || reachKing(arr, row - 1, col + 2)
                || reachKing(arr, row + 2, col + 1)
                || reachKing(arr, row - 1, col - 2)
                || reachKing(arr, row + 1, col - 2)
                || reachKing(arr, row - 2, col - 1)
                || reachKing(arr, row + 2, col - 1)
                || reachKing(arr, row + 2, col + 1)
                || reachKing(arr, row - 2, col + 1);
    }

    private static void print(char[][] arr) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        char[][] arr = {
            {' ', ' ', 'X', 'K', 'X', ' ', ' ', ' '},
            {' ', 'X', 'X', 'X', 'X', 'X', ' ', ' '},
            {' ', ' ', 'X', 'X', 'X', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', 'X', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', ' ', ' ', ' ', ' ', 'X', ' ', ' '},};
        System.out.println(reachKing(arr, 6, 7));
        print(arr);
    }

}
