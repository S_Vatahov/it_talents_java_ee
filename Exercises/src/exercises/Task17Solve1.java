/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

import java.util.ArrayList;

/**
 *
 * @author slavi
 */
public class Task17Solve1 {

    private static void mazze(char[][] mazze, int row, int col, ArrayList<String> allPaths, String path, char direction) {
        if (row < 0 || row >= mazze.length || col < 0 || col >= mazze[row].length) {
            return;
        }
        if (mazze[row][col] == 'e') {
            allPaths.add(path);
        }
        if (mazze[row][col] != ' ') {
            return;
        }
        path = path.concat(Character.toString(direction));
        mazze[row][col] = 's';

        mazze(mazze, row + 1, col, allPaths, path, 'D');
        mazze(mazze, row - 1, col, allPaths, path, 'U');
        mazze(mazze, row, col + 1, allPaths, path, 'R');
        mazze(mazze, row, col - 1, allPaths, path, 'L');

        mazze[row][col] = ' ';
    }

    public static void main(String[] args) {
        char[][] mazze = {
            {' ', ' ', ' ', ' ', ' ', ' ', ' '},
            {' ', '*', ' ', '*', '*', '*', ' '},
            {' ', '*', ' ', ' ', ' ', '*', ' '},
            {' ', '*', '*', '*', ' ', '*', ' '},
            {' ', ' ', ' ', ' ', ' ', ' ', 'e'}
        };
        ArrayList<String> allPaths = new ArrayList<>();
        mazze(mazze, 0, 0, allPaths, " ", ' ');
        System.out.println(allPaths.toString());
    }
}
