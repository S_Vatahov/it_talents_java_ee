/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task12 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char[] cardsArr = {'2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'};

        int counter = 0;
        boolean card1 = false;
        boolean card2 = false;

        while (true) {
            counter++;

            System.out.println("Enter two cards separetet by interval ");
            String s1 = input.nextLine();
            String[] s2 = s1.split(" ");

            for (int i = 0; i < cardsArr.length; i++) {
                if (s2[0].charAt(0) == cardsArr[i]) {
                    card1 = true;
                }
                if (s2[1].charAt(0) == cardsArr[i]) {
                    card2 = true;
                }
            }
            if (card1 && card2) {
                System.out.println(s2[0].charAt(0) + " " + s2[1].charAt(0));
                if (s2[0].charAt(0) == 'A' && s2[1].charAt(0) == 'A') {
                    break;
                }
            } else {
                System.out.println(" Invalid Card " + s2[0].charAt(0) + " " + s2[1].charAt(0));
            }
        }
        System.out.println("Total tries " + counter);

    }
}
