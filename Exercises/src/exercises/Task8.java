/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task8 {
    
    private static void printRatio(String s1) {
        int lowerCounter = 0;
        int uperCounter = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (Character.isUpperCase(s1.charAt(i))) {
                uperCounter++;
            } else {
                lowerCounter++;
            }
        }
        for (int i = 2; i < uperCounter; i++) {
            if (lowerCounter % i == 0 && uperCounter % i == 0) {
                lowerCounter /= i;
                uperCounter /= i;
                i--;
            }
        }
        System.out.println(lowerCounter + ":" + uperCounter);
    }
    
    public static void main(String[] args) {
        String s1 = "sdfs DSF dasfSDdasf dafadz azdfF ";
        printRatio(s1);
    }
}
