/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task5 {
    
    private static char[][] generateNumbers(char[][] arr) {
     
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                int minesCounter = 0;
                if (arr[i][j] == '*') {
                    continue;
                }
                for (int row = -1; row <= 1; row++) {
                    for (int col = -1; col <= 1; col++) {
                        if (i + row != -1 && i + row < arr.length && j + col != -1 && j + col < arr[i].length) {
                            if (arr[i + row][j + col] == '*') {
                                minesCounter++;
                            }
                            
                        }
                    }
                }
                arr[i][j] = Integer.toString(minesCounter).charAt(0);
                
            }
        }
        return arr;
    }
    
    private static void print(char[][] arr) {
        for (char[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
        
    }
    
    public static void main(String[] args) {
        char[][] arr = {
            {'*', ' ', '*', ' '},
            {' ', '*', ' ', ' '},
            {' ', '*', ' ', ' '},
            {'*', ' ', '*', ' '}};
        arr = generateNumbers(arr);
        print(arr);
        
    }
}
