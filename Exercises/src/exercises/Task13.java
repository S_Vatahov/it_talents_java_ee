/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises;

/**
 *
 * @author slavi
 */
public class Task13 {

    private static void printLongestSentence(String s1) {
        String[] s2 = s1.split("\\.");
        int index = 0;
        for (int i = 1; i < s2.length; i++) {
            if (s2[i].length() > s2[index].length()) {
                index = i;
            }
        }
        System.out.println(s2[index]+".");
        System.out.println(s2.length + " sentence in the text.");
    }

    public static void main(String[] args) {
        String s1 = "Today is a good day for test. Sun is shining. The students are happy. The birds are\n"
                + "blue.";
        printLongestSentence(s1);
    }
}
