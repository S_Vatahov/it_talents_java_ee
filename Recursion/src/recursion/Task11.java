/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task11 {

    private static void printAllElements(int[] arr, int index) {
        if (index == arr.length) {
            return;
        }
        System.out.print(arr[index] + "|");
        printAllElements(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 2, 23234, 3, 4, 5};
        printAllElements(arr, 0);
    }

}
