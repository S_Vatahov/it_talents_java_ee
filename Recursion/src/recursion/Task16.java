/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task16 {

    private static int frequency(int[] arr, int index, int currFrequency, int frequency) {
        if (index == arr.length - 1) {
            if (currFrequency > frequency) {
                frequency = currFrequency;
            }
            return frequency;
        }
        if (arr[index] == arr[index + 1]) {
            currFrequency++;
        } else {
            if (currFrequency > frequency) {
                frequency = currFrequency;
            }
            currFrequency = 1;
        }
        return frequency(arr, index + 1, currFrequency, frequency);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 8, 8, 8, 8};
        System.out.println(frequency(arr, 0, 1, 1));
    }
}
