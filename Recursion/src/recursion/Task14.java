/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

import java.util.Arrays;

/**
 *
 * @author slavi
 */
public class Task14 {
    
    private static void reverse(int[]arr,int index){
        if(index==arr.length/2){
            return;
        }
        int temp = arr[index];
        arr[index]= arr[arr.length-index-1];
        arr[arr.length-index-1]= temp;
        reverse(arr, index+1);
    }

    public static void main(String[] args) {
        int[] arr = {5, 1, 6, 10, 2, 3, 6, 50, -7, 4};
        reverse(arr, 0);
        System.out.println(Arrays.toString(arr));
    }
}
