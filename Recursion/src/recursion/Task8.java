/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task8 {
    
    private static int sumDig(int number, int sum) {
        if (number == 0) {
            return sum;
        }
        sum += number % 10;
        return sumDig(number / 10, sum);
    }
    
    public static void main(String[] args) {
        System.out.println(sumDig(12345, 0));
    }
}
