/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task13 {

    private static int minElement(int[] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int min = minElement(arr, index + 1);
        if (min < arr[index]) {
            return min;
        }
        return arr[index];
    }

    private static int maxElement(int[] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int max = maxElement(arr, index + 1);
        if (max > arr[index]) {
            return max;
        }
        return arr[index];
    }

    public static void main(String[] args) {
        int[] arr = {5, 1, 6, 10, 2, 3, 6, 50, -7, 4};

        System.out.println("Min " + minElement(arr, 0));
        System.out.println("Max " + maxElement(arr, 0));
    }
}
