/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task17 {

    private static int sum(int[][] arr, int row, int col) {
        if (row == arr.length - 1 && col == arr[row].length - 1) {
            return arr[row][col];
        }
        if (col < arr.length - 1) {
            return arr[row][col] + sum(arr, row, col + 1);
        } else {
            return arr[row][col] + sum(arr, row + 1, 0);
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        System.out.println(sum(arr, 0, 0));
    }
}
