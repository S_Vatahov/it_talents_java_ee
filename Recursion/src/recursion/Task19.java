/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task19 {

    private static boolean contains(int[][] arr, int row, int col, int element) {
        if (arr[row][col] == element) {
            return true;
        }
        if (row == arr.length - 1 && col == arr[row].length - 1) {
            return false;
        }

        if (col < arr[row].length - 1) {
            return contains(arr, row, col + 1, element);
        } else {
            return contains(arr, row + 1, 0, element);
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        System.out.println(contains(arr, 0, 0, 9));
    }
}
