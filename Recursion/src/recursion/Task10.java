/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static int fibunachi(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fibunachi(n - 1) + fibunachi(n - 2);
    }

    public static void main(String[] args) {
        System.out.println(fibunachi(10));
    }
}
