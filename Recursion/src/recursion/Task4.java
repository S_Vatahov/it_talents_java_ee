/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static int sum(int n, int x) {
        if (n == x) {
            return n;
        }
        return n + sum(n + 1, x);
    }

    public static void main(String[] args) {
        System.out.println(sum(1, 10));
    }
}
