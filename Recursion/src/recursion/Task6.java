/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task6 {
    
    private static int reverse(int num, int number) {
        int dig = num % 10;
        num /= 10;
        if (num == 0) {
            return number * 10 + dig;
        }
        return reverse(num, number * 10 + dig);
    }
    
    public static void main(String[] args) {
        System.out.println(reverse(1234, 0));
    }
}
