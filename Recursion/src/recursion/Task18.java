/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task18 {

    private static int average(int[][] arr, int row, int col, int sum) {
        if (row == arr.length - 1 && col == arr[row].length - 1) {
            sum += arr[row][col];
            return sum / (arr.length * arr[row].length);
        }
        sum += arr[row][col];
        if (col < arr.length - 1) {
            return average(arr, row, col + 1, sum);
        } else {
            return average(arr, row + 1, 0, sum);
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        System.out.println(average(arr, 0, 0, 0));
    }
}
