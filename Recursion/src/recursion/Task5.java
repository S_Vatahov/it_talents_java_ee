/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static int sum(int n, int x, int sum, boolean even) {
        if (n > x) {
            return sum;
        }
        if (even) {
            if (n % 2 == 0) {
                sum += n;
            }
        } else {
            if (n % 2 != 0) {
                sum += n;
            }
        }
        return sum(n+1, x, sum, even);
    }

    public static void main(String[] args) {
        System.out.println(sum(1, 100, 0, true));
    }
}
