/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static int reverse(int num, int number) {
        int dig = num % 10;
        num /= 10;
        if (num == 0) {
            return number * 10 + dig;
        }
        return reverse(num, number * 10 + dig);
    }

    private static boolean isPalindrom(int num1, int num2) {
        if (num1 == 0) {
            return true;
        }
        if (num1 % 10 != num2 % 10) {
            return false;
        }
        return isPalindrom(num1 / 10, num2 / 10);
    }

    public static void main(String[] args) {
        int number = 121;
        int number2 = reverse(number, 0);
        System.out.println(isPalindrom(number, number2));
    }
}
