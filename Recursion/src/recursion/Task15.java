/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task15 {

    private static int mostDuplicatedValue(int[] arr, int index, int value, int maxCouner) {
        if (index == arr.length) {
            System.out.println(maxCouner+" Times");
            return value;
        }
        int counter = duplicateCounter(arr, arr[index], 0, 0);
        if (counter > maxCouner) {
            maxCouner = counter;
            value = arr[index];
        }
        return mostDuplicatedValue(arr, index + 1, value, maxCouner);
    }

    private static int duplicateCounter(int[] arr, int element, int index, int counter) {
        if (index == arr.length) {
            return counter;
        }
        if (arr[index] == element) {
            counter++;
        }
        return duplicateCounter(arr, element, index + 1, counter);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 4, 5, 4, 1, 2, 3, 4, 5,4};
        System.out.println(mostDuplicatedValue(arr, 0, 0, 0));
    }
}
