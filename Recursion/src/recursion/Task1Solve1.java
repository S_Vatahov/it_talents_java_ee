/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task1Solve1 {

    private static int pow(int x, int y) {
        if (y == 1) {
            return x;
        }
        return x * pow(x, y - 1);
    }

    public static void main(String[] args) {
        System.out.println(pow(2, 3));
    }

}
