/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author slavi
 */
public class Task2 {
    
    private static void printNumbs(int n, int x) {
        if (n > x) {
            return;
        }
        System.out.print(n + "|");
        printNumbs(n + 1, x);
    }
    
    public static void main(String[] args) {
        printNumbs(1, 10);
    }
}
