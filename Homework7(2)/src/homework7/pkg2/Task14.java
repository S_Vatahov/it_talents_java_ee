/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task14 {

    private static int getSecondLargestElement(int[] arr) {
        int min = arr[0];
        int secondMin = Integer.MAX_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > min && arr[i] < secondMin) {
                secondMin = arr[i];
            }
        }
        return secondMin;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 55, 32, 88};
        System.out.println("Second min is " + getSecondLargestElement(arr));
    }
}
