/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task30 {
    
    private static int swapDiagonals(int[][] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        int temp = arr[index][index];
        arr[index][index] = arr[index][arr.length - index - 1];
        arr[index][arr.length - index - 1] = temp;
        return swapDiagonals(arr, index + 1);
    }
    
    private static void printMatrix(int[][] arr) {
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        swapDiagonals(arr1, 0);
        printMatrix(arr1);
    }
}
