/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task31 {

    private static boolean isUpperTriangularMatrix(int[][] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i][j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {0, 5, 6},
            {0, 0, 9}
        };
        System.out.println("Is upper triangular matrix ? - " + isUpperTriangularMatrix(arr1));
    }
}
