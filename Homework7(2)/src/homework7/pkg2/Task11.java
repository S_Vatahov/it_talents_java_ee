/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task11 {

    private static int printCommonElements(int[] arr1, int[] arr2, int index, int minLength) {

        if (index == minLength) {
            return -1;
        }
        if (arr1[index] == arr2[index]) {
            System.out.print(arr1[index] + "|");
        }
        return printCommonElements(arr1, arr2, index + 1, minLength);
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 2, 4, 2, 4, 5, 6, 5,};
        int[] arr2 = {3, 2, 3, 4, 5, 7, 4, 8, 9, 5, 6};
        printCommonElements(arr1, arr2, 0, arr1.length);
    }
}
