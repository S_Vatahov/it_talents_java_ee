/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static int indexOf(int[] arr, int index, int value) {
        if (index == arr.length) {
            return -1;
        }
        if (arr[index] == value) {
            return index;
        }
        return indexOf(arr, index + 1, value);
    }

    public static void main(String[] args) {
        int b = 4;
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        System.out.println("Index of element is " + indexOf(arr, 0, b));
    }
}
