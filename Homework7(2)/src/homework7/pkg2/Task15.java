/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task15 {
    
    private static void printAllNumEqualsToSpecVal(int[] arr, int num) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] + arr[j] == num) {
                    System.out.println(arr[i] + " + " + arr[j] + " = " + num);
                }
            }
        }
    }
    
    public static void main(String[] args) {
        int num = 12;
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        printAllNumEqualsToSpecVal(arr, num);
    }
}
