/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task18 {

    private static void printNumbFrquency(int[] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            int frequencyCounter = 1;
            if (arr[i] == 0) {
                continue;
            }
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j] && arr[j] != 0) {
                    frequencyCounter++;
                }
            }
            System.out.println(arr[i] + "-" + frequencyCounter);
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 1, 1, 3, 4, 1, 4, 5, 6, 9, 7, 8, 9};
        printNumbFrquency(arr1);
    }
}
