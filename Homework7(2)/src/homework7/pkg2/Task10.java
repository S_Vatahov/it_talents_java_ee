/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

import java.util.TreeSet;

/**
 *
 * @author slavi
 */
public class Task10 {

    private static void printDuplicate(int[] arr) {
        // Използвам множество за да нямам повтарящи се елементи при принтирането,понеже ако има повече от
        // 2 повтарящи се получава припокриване.
        TreeSet<Integer> set = new TreeSet<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    set.add(arr[i]);
                }
            }

        }
        System.out.println(set.toString());
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 2, 4, 2, 4, 5, 6, 5};
        printDuplicate(arr1);
    }
}
