/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task20 {

    private static int getNumberOfNegativeElements(int[] arr, int index, int counter) {
        if (index == arr.length) {
            return counter;
        }
        if (arr[index] < 0) {
            counter++;
        }
        return getNumberOfNegativeElements(arr, index + 1, counter);
    }

    public static void main(String[] args) {
        int[] arr1 = {-1, 2, 1, 1, 3, 4, -1, 4, 5, 6, 9, 7, -8, -9};
        System.out.println("The number of negative elements is - " + getNumberOfNegativeElements(arr1, 0, 0));
    }
}
