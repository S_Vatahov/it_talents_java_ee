/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task28 {

    private static int oppositeDiagonalSum(int[][] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index][arr.length - index - 1];
        }
        return arr[index][arr.length - index - 1] + oppositeDiagonalSum(arr, index + 1);
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        System.out.println("The opposite diagonal sum is " + oppositeDiagonalSum(arr1, 0));
    }
}
