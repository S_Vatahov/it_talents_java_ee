/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task5 {

    private static int remove(int[] arr, int index, int element) {
        if (index == arr.length - 1) {
            if (arr[index] == element) {
                arr[index] = 0;
            }
            return element;
        }
        if (arr[index] == element) {
            arr[index] = 0;
        }
        if (arr[index] == 0) {
            arr[index] = arr[index + 1];
            arr[index + 1] = 0;
        }
        return remove(arr, index + 1, element);
    }

    private static int printArr(int[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        System.out.print(arr[index] + "|");
        return printArr(arr, index + 1);
    }

    public static void main(String[] args) {
        int b = 8;
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        remove(arr, 0, b);
        printArr(arr, 0);
    }
}
