/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task27 {

    private static int majorDiagonalSum(int[][] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index][index];
        }
        return arr[index][index] + majorDiagonalSum(arr, index + 1);
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        System.out.println("The major diagonal sum is " + majorDiagonalSum(arr1, 0));
    }
}
