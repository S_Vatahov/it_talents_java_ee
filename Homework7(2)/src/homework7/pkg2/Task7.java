/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task7 {

    private static int[] add(int[] arr, int element, int index) {
        if (index > arr.length) {
            int[] copyArray = new int[index + 1];
            System.arraycopy(arr, 0, copyArray, 0, arr.length - 1);
            copyArray[index] = element;
            return copyArray;
        }
        int[] copyArray = new int[arr.length + 1];
        System.arraycopy(arr, 0, copyArray, 0, index);
        copyArray[index] = element;
        System.arraycopy(arr, index, copyArray, index + 1, arr.length - index);
        return copyArray;

    }

    private static int printArr(int[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        System.out.print(arr[index] + "|");
        return printArr(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        arr1 = add(arr1, 54, 4);
        printArr(arr1, 0);
    }
}
