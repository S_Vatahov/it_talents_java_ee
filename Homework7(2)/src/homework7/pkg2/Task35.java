/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task35 {

    private static boolean isIdentityMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][i] != 1) {
                return false;
            }
            for (int j = 0; j < arr.length; j++) {
                if (arr[i][j] != 0 && i != j) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
        };
        System.out.println("Identity matrix ?? " + isIdentityMatrix(arr1));
    }
}
