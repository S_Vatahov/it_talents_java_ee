/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task19 {

    static Scanner input = new Scanner(System.in);

    private static void fillArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter arr[" + i + "] = ");
            arr[i] = input.nextInt();
        }
    }

    private static void countEvenAndOdd(int[] arr) {
        int evenCounter = 0;
        int oddCounter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                evenCounter++;
            } else {
                oddCounter++;
            }
        }
        System.out.println("Total even elements is " + evenCounter);
        System.out.println("Total odd elements is " + oddCounter);
    }

    public static void main(String[] args) {

        int n;
        do {
            System.out.print("Enter array length = ");
            n = input.nextInt();
        } while (n <= 0);
        int[] arr = new int[n];
        fillArray(arr);
        countEvenAndOdd(arr);

    }
}
