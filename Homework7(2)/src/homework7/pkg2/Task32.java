/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task32 {

    private static boolean isLowerTriangularMatrix(int[][] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i][j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 0, 0},
            {4, 5, 0},
            {7, 8, 9}
        };
        System.out.println("Is lower triangular matrix? - " + isLowerTriangularMatrix(arr1));
    }
}
