/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task29 {

    private static void printRowAndColSum(int[][] arr) {

        //Само за квадратна матрица
        for (int i = 0; i < arr.length; i++) {
            int rowSum = 0;
            int colSum = 0;
            for (int j = 0; j < arr.length; j++) {
                rowSum += arr[i][j];
                colSum += arr[j][i];
            }
            System.out.println("Sum of row " + (i+1) + " = " + rowSum);
            System.out.println("Sum of col " + (i+1)+ " = " + colSum);
        }
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        printRowAndColSum(arr1);
    }
}
