/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task6 {

    private static int copyArray(int[] arr1, int[] arr2, int index) {
        if (index == arr1.length) {
            return -1;
        }
        arr2[index] = arr1[index];
        return copyArray(arr1, arr2, index + 1);
    }

    private static int printArr(int[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        System.out.print(arr[index] + "|");
        return printArr(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] arr2 = new int[arr1.length];
        copyArray(arr1, arr2, 0);
        printArr(arr2, 0);
    }
}
