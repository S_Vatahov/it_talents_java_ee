/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task9 {

    private static int reverese(int[] arr, int[] arr2, int index1) {
        if (index1 == arr.length - 1) {
            return arr[index1];
        }
        arr2[arr.length - index1 - 1] = reverese(arr, arr2, index1 + 1);
        return arr[index1];
    }

    private static int printArray(int[] arr, int index) {
        if (index == arr.length) {
            return -1;
        }
        System.out.print(arr[index] + "|");
        return printArray(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr1 = {-1, 2, -3, 4, 55, 6, 7, 8, 9};
        int[] arr2 = new int[arr1.length];
        reverese(arr1, arr2, 0);
        printArray(arr1, 0);
    }
}
