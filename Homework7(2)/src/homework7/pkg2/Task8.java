/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task8 {

    private static int maxValue(int[] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int max = maxValue(arr, index + 1);
        if (max > arr[index]) {
            return max;
        } else {
            return arr[index];
        }
    }

    private static int minValue(int[] arr, int index) {
        if (index == arr.length - 1) {
            return arr[index];
        }
        int min = minValue(arr, index + 1);
        if (min < arr[index]) {
            return min;
        } else {
            return arr[index];
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {-1, 2, -3, 4, 55, 6, 7, 8, 9};
        System.out.println("Max value is " + maxValue(arr1, 0));
        System.out.println("Min value is " + minValue(arr1, 0));
    }
}
