/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task17 {

    private static void printUniqueNumbs(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            boolean flag = false;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j] && i != j) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                System.out.print(arr[i] + "|");
            }
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 4, 5, 6, 9, 7, 8, 9};
        printUniqueNumbs(arr1);
    }
}
