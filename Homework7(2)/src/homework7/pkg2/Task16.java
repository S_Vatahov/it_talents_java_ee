/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task16 {

    private static boolean isEquals(int[] arr1, int[] arr2, int index) {
        if (arr1.length != arr2.length) {
            return false;
        }
        if (index == arr1.length) {
            return true;
        }
        if (arr1[index] != arr2[index]) {
            return false;
        }
        return isEquals(arr1, arr2, index + 1);
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        int[] arr2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        System.out.println("Is equals ? - " + isEquals(arr1, arr2, 0));
    }
}
