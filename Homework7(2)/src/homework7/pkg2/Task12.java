/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task12 {

    private static int[] removeDuplicate(int[] arr) {
        int index = 0;
        int[] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    arr[i] = 0;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                continue;
            }
            arr2[index++] = arr[i];
        }
        return arr2;
    }

    private static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "|");
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 2, 4, 2, 4, 5, 6, 5,};
        arr1 = removeDuplicate(arr1);
        printArr(arr1);
    }
}
