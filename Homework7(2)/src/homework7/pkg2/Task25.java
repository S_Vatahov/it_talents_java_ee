/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task25 {

    private static int[][] multiplicationMatrix(int[][] arr1, int[][] arr2) {
        int[][] arr3 = new int[arr1.length][arr1[0].length];
        int indexI = 0;
        for (int i = 0; i < arr1.length; i++) {
            int indexJ = 0;
            for (int j = 0; j < arr1[i].length; j++) {
                for (int k = 0; k < arr1.length; k++) {
                    arr3[i][j] += arr1[indexI][k] * arr2[k][indexJ];

                }
                indexJ++;
            }
            indexI++;
        }
        return arr3;
    }

    private static void printMatrix(int[][] arr) {
        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        int[][] arr2 = {
            {9, 8, 7},
            {6, 5, 4},
            {3, 2, 1}
        };
        int[][] arr3 = new int[arr1.length][arr1[0].length];
        arr3 = multiplicationMatrix(arr1, arr2);
        printMatrix(arr3);
    }
}
