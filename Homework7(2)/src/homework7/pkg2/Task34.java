/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task34 {

    private static int[][] transposeMatrix(int[][] arr) {
        int[][] matrix = new int[arr.length][arr[0].length];
        if (arr.length != arr[0].length) {
            return matrix;
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                matrix[i][j] = arr[j][i];
            }
        }
        return matrix;
    }

    private static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] arr1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        arr1=transposeMatrix(arr1);
        printMatrix(arr1);
    }
}
