/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task21 {

    static Scanner input = new Scanner(System.in);

    private static void fillArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter arr[" + i + "] = ");
            arr[i] = input.nextInt();
        }
    }

    private static void separete(int[] arr, int[] arr1, int[] arr2) {
        int index1 = 0;
        int index2 = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                arr1[index1++] = arr[i];
            } else {
                arr2[index2++] = arr[i];
            }
        }

    }

    private static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                continue;
            }
            System.out.print(arr[i] + "|");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        int n;
        do {
            System.out.print("Enter array length = ");
            n = input.nextInt();
        } while (n <= 0);
        int[] arr = new int[n];
        int[] arr1 = new int[n];
        int[] arr2 = new int[n];
        fillArray(arr);
        separete(arr, arr1, arr2);
        printArray(arr);
        printArray(arr1);
        printArray(arr2);

    }
}
