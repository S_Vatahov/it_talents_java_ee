/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task2 {
    
    private static int average(int[] arr, int index, int sum) {
        if (index == arr.length) {
            return sum / arr.length;
        }
        sum += arr[index];
        return average(arr, index + 1, sum);
    }
    
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println("The average is " + average(arr, 0, 0));
        
    }
}
