/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework7.pkg2;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static boolean contains(int[] arr, int index, int value) {
        if (index == arr.length) {
            return false;
        }
        if (arr[index] == value) {
            return true;
        }

        return contains(arr, index + 1, value);
    }

    public static void main(String[] args) {
        int b = 9;
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        System.out.println("Contain the value ??? - " + contains(arr, 0, b));

    }
}
