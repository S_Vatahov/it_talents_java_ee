/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework18;

import java.text.DecimalFormat;

/**
 *
 * @author slavi
 */
public class Employee extends Person {

    private double daySalary;

    Employee(String name, int age, boolean isMale, double daySalary) {
        super(name, age, isMale);
        this.setDaySalary(daySalary);
    }

    double getDaySalary() {
        return daySalary;
    }

    final void setDaySalary(double daySalary) {
        if (daySalary > 0) {
            this.daySalary = daySalary;
        }
    }

    double calculateOvertime(double hours) {
        if (this.getAge() < 18) {
            return 0;
        }
        if (hours < 0) {
            return 0;
        }
        return (this.getDaySalary() / 8) * (1.5 * hours);

    }

    @Override
    void showPersonInfo() {
        DecimalFormat format = new DecimalFormat("###.##");
        String leftAlginFormat = "| %-8s | %-3d | %-6b | %-11s |";
        System.out.format("+----------+-----+--------+-------------+%n");
        System.out.format(Person.ANSI_GREEN_FILL + "|   Name   | Age | isMale |  Day salary |%n");
        System.out.format("+----------+-----+--------+-------------+%n");
        System.out.format(Person.ANSI_CYAN_FILL + leftAlginFormat, this.getName(), this.getAge(), this.isIsMale(), format.format(this.getDaySalary()));
        System.out.format("%n+----------+-----+--------+-------------+%n");
    }

}
