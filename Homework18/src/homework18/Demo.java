/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework18;

/**
 *
 * @author slavi
 */
public class Demo {

    private static void shoPersonsInfo(Person[] persons) {
        for (Person person : persons) {
            person.showPersonInfo();

            /* Понеже в условието е казано методите показващи информацията за Studen, Person и Employee да са различни 
            а аз съм предефинирал метода от Person и става така че от това долу няма смисъл.
            
            if (person instanceof Student) {
                ((Student) person).showPersonInfo();
            } else if (person instanceof Employee) {
                ((Employee) person).showPersonInfo();
            } else {
                person.showPersonInfo();
            }
             */
        }
    }

    private static void showOvertimeBonus(Person[] persons) {
        for (Person person : persons) {
            if (person instanceof Employee) {
                System.out.println("Извънредни на " + person.getName() + " " + ((Employee) person).calculateOvertime(2) + " \uD83D\uDCB0");
            }

        }
    }

    public static void main(String[] args) {
        Person[] persons = {new Person("Pesho", 18, true), new Person("Ginka", 20, false), new Student("Ivan", 20, true, 6),
            new Student("Puhi", 19, false, 6), new Employee("Ali", 35, true, 60), new Employee("Fikret", 50, false, 45.850001),
            new Person(null, 18, true), new Person(null, 18, true), new Person(null, 18, true), new Person(null, 18, true)};

        shoPersonsInfo(persons);
        showOvertimeBonus(persons);
        
        
        /*
        И отговор на въпроса след задачата.
        Ако дефинираме конструктор без параметри ще възникне компилационна грешка защото в superclass-а на този клас
        няма конструктор без параметри. Грешката може да се оправи по 2 начина:
        1 Като дефинираме конструктор без параметри в класа родител.
        2 Като в конструктора без параметри на класа Student извикаме super конструктора на базовия му клас и му зададем 
        default-ни параметри.
        */

    }
}
