/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework18;

import java.text.DecimalFormat;

/**
 *
 * @author slavi
 */
public class Student extends Person {

    private double score;
   

    Student(String name, int age, boolean isMale, double score) {
        super(name, age, isMale);
        this.setScore(score);
       
    }

    double getScore() {
        return score;
    }

    final void setScore(double score) {
        if (score >= 2 && score <= 6) {
            this.score = score;
        }
        this.score = score;
    }

    @Override
    void showPersonInfo() {
        DecimalFormat df = new DecimalFormat("#.##");

        String leftAlginFormat = "| %-8s | %-3d | %-6b | %-6s |";
        System.out.format("+----------+-----+--------+--------+%n");
        System.out.format(Person.ANSI_GREEN_FILL + "|   Name   | Age | isMale |  Score |%n");
        System.out.format("+----------+-----+--------+--------+%n");
        System.out.format(Person.ANSI_CYAN_FILL + leftAlginFormat, this.getName(), this.getAge(), this.isIsMale(), df.format(this.getScore()));
        System.out.format("%n+----------+-----+--------+--------+%n");
    }

}
