/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework18;

/**
 *
 * @author slavi
 */
public class Person {

    private final String name;
    private int age;
    private final boolean isMale;

    protected static final String ANSI_GREEN_FILL = "\u001B[42m";
    protected static final String ANSI_CYAN_FILL = "\u001B[46m";

    Person(String name, int age, boolean isMale) {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            this.name = "Unnamed";
        }
        this.setAge(age);
        this.isMale = isMale;
    }

    final String getName() {
        return name;
    }

    final int getAge() {
        return age;
    }

    final boolean isIsMale() {
        return isMale;
    }

    final void setAge(int age) {
        if (age >= 0 && age <= 150) {
            this.age = age;
        }
    }

    void showPersonInfo() {
        String leftAlginFormat = "| %-8s | %-3d | %-6b |%n";
        System.out.format("+----------+-----+--------+%n");
        System.out.format(ANSI_GREEN_FILL + "|   Name   | Age | isMale |%n");
        System.out.format("+----------+-----+--------+%n");
        System.out.format(ANSI_CYAN_FILL + leftAlginFormat, this.getName(), this.getAge(), this.isIsMale());
        System.out.format("+----------+-----+--------+%n");
    }

}
