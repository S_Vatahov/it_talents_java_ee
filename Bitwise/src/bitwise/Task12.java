/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task12 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();
        int totalOneCounter = 0;
        int totalZeroCounter = 0;
        boolean isNegative = false;
        if (number < 0) {
            isNegative = true;
            number *= -1;
        }
        int counter =0;
        while (counter < 32) {
            if ((number & 1) == 1) {
                totalOneCounter++;
            } else {
                totalZeroCounter++;
            }
            counter++;
            number >>= 1;
        }
        if (isNegative) {
            totalOneCounter++;
            totalZeroCounter--;
        }
        System.out.println("One = " + totalOneCounter);
        System.out.println("Zero = " + totalZeroCounter);
    }
}
