/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task13 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();
        int[] arr = new int[32];
        boolean isNegative = false;
        if (number < 0) {
            isNegative = true;
            number *= -1;
        }
        int index = 0;
        while (number > 0) {
            int dig = number & 1;
            number >>= 1;
            arr[index++] = dig;
        }
        if (isNegative) {
            arr[arr.length - 1] = 1;
        }

        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.print(arr[i] + "|");
        }
    }
}
