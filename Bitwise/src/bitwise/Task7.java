/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task7 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();

        if (number < 0) {
            System.out.println("31");
        } else {
            int counter = 0;
            int max = 0;
            while (number > 0) {

                if ((number & 1) == 1) {
                    max = counter;
                }
                counter++;
                number >>= 1;
            }

            System.out.println(max);
        }
    }
}
