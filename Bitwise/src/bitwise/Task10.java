/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task10 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();

        int counter = 0;
        int maxZerro = 0;
        while (counter < 32) {
            if ((number & 1) == 0) {
                maxZerro++;
            } else {
                maxZerro = 0;
            }
            counter++;
            number >>= 1;
        }
        System.out.println(maxZerro);
    }
}
