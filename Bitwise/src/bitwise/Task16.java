/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

/**
 *
 * @author slavi
 */
public class Task16 {

    private static int multiplication(int x, int y, int result) {
        if (y == 0) {
            return result;
        }
        if ((y & 1) == 1) {
            result += x;
        }
        return multiplication(x << 1, y >> 1, result);
    }

    public static void main(String[] args) {
        System.out.println(multiplication(4, 5, 0));
    }
}
