/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task6 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();
        int n = input.nextInt();
        System.out.println("Before " + Integer.toBinaryString(number));
        number ^= 1 << n;
        System.out.println("After  " + Integer.toBinaryString(number));
    }
}
