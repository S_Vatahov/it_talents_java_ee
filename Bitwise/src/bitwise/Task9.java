/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

import java.util.Scanner;

/**
 *
 * @author slavi
 */
public class Task9 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number = ");
        int number = input.nextInt();
        int counter = 0;
        int flag = 0;
        while (flag == 0) {
            flag = number & 1;
            counter++;
            number >>= 1;
        }
        System.out.println(counter-1);
    }
}
