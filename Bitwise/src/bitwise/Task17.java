/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

/**
 *
 * @author slavi
 */
public class Task17 {

    private static int substract(int x, int y) {
        if (y == 0) {
            return x;
        }
        return substract(x ^ y, (~x & y) << 1);
    }

    public static void main(String[] args) {
        System.out.println(substract(5, 8));
    }
}
