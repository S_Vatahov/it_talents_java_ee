/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitwise;

/**
 *
 * @author slavi
 */
public class Task14 {

    public static void main(String[] args) {
        int a = 22;
        int b = 42;
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        
        System.out.println(a);
        System.out.println(b);
    }
}
