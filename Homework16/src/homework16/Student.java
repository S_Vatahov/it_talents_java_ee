/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16;

/**
 *
 * @author slavi
 */
public class Student {

    private String name;
    private String subject;
    private double grade;
    private int yearInCollage;
    private int age;
    private boolean isDegree;
    private int money;

    Student() {
        this.grade = 4.0;
        this.yearInCollage = 1;
        this.isDegree = false;
        this.money = 0;
    }

    Student(String name, String subject, int age) {
        this();

        this.setName(name);
        this.setSubject(subject);
        this.setAge(age);
    }

    void upYear() {
        if (isDegree) {
            System.err.println("The student has graduated!");
            return;
        }
        this.yearInCollage++;
        if (this.yearInCollage == 4) {
            isDegree = true;
        }
    }

    double receiveScholarship(double min, int amount) {
        if (this.grade < min || this.age > 30) {
            System.err.println("The student does not meet the criteria!");
            return this.money;
        }
        this.money += amount;
        return this.money;
    }

    final void setName(String name) {
        if (name == null || name.equals("")) {
            System.err.println("Incorrect name !");
            return;
        }
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setYearInCollage(int yearInCollage) {
        if (yearInCollage < 0 || yearInCollage > 4) {
            System.err.println("Incorrect year in collage");
            return;
        }
        this.yearInCollage = yearInCollage;
    }

    int getYearInCollage() {
        return yearInCollage;
    }

    final void setAge(int age) {
        if (age < 16 || age > 100) {
            System.err.println("Incorrect age");
            return;
        }
        this.age = age;
    }

    int getAge() {
        return age;
    }

    void setIsDegree(boolean isDegree) {
        this.isDegree = isDegree;
    }

    boolean isIsDegree() {
        return isDegree;
    }

    void setMoney(int money) {
        if (money < 0) {
            System.err.println("Incorrect money");
            return;
        }
        this.money = money;
    }

    int getMoney() {
        return money;
    }

    void setGrade(double grade) {
        if (grade < 2 || grade > 6) {
            System.err.println("Incorrect grade");
            return;
        }
        this.grade = grade;
    }

    double getGrade() {
        return grade;
    }

    final void setSubject(String subject) {
        if (subject == null || subject.equals("")) {
            System.err.println("Incorrect subject");
            return;
        }
        this.subject = subject;
    }

    String getSubject() {
        return subject;
    }
}
