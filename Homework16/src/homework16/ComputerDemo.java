/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16;

/**
 *
 * @author slavi
 */
public class ComputerDemo {

    public static void main(String[] args) {
        Computer pc1 = new Computer(2017, 2000, 2000, 1800, ComputerType.NOTEBOOK, OperationSystem.WINDOWS);
        Computer pc2 = new Computer(2017, 3000, 4000, 3800, ComputerType.DESKTOP, OperationSystem.LINUX);
        
        System.out.println(pc2.comparePrice(pc1));
        System.out.println(pc1.comparePrice(pc1));
        System.out.println(pc1.comparePrice(pc2));
    }
}
