/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16;

/**
 *
 * @author slavi
 */
public class Collage {

    public static void main(String[] args) {
        Student ginka = new Student("Ginka", "Math1", 19);
        Student penka = new Student("Penka", "Math1", 19);
        Student ivan = new Student("Ivan", "Math1", 20);
        Student stoqn = new Student("Stoqn", "Math1", 20);
        Student ivo = new Student("Ivo", "Math1", 21);
        Student dragan = new Student("Dragan", "Math2", 21);
        Student petkan = new Student("Petkan", "Math2", 21);

        Student pesho = new Student("Pesho", "Literature", 20);
        Student puhi = new Student("Puhi", "Literature", 19);

        Student georgi = new Student("Georgi", "History", 23);
        Student dimitar = new Student("Dimitar", "History", 22);

        Student aliRaza = new Student("Ali Raza", "Medical", 80);
        Student ivanIvanov = new Student("Ivan Ivanov", "Medical", 21);
        Student georgiGeorgiev = new Student("Georgi Georgiev", "Medical", 21);
        Student aliBaba = new Student("Ali Baba", "Medical", 22);
        Student perhunde = new Student("Puhi", "Medical", 23);

        ivanIvanov.receiveScholarship(4.0, 750);
        georgiGeorgiev.receiveScholarship(4.0, 750);
        aliBaba.setGrade(3.25);
        perhunde.setGrade(5.25);
        perhunde.receiveScholarship(5.0, 1200);
        aliRaza.setGrade(6.0);
        ginka.setGrade(5.0);
        aliRaza.upYear();
        aliRaza.upYear();
        aliRaza.upYear();
        penka.upYear();
        penka.upYear();
        penka.upYear();
        pesho.upYear();
        puhi.upYear();
        pesho.setGrade(6.0);

        ginka.receiveScholarship(4.0, 600);
        penka.receiveScholarship(4.0, 600);
        ivan.receiveScholarship(4.0, 352);
        puhi.receiveScholarship(4.0, 600);

        StudentGroup math1 = new StudentGroup("Math1");
        math1.addStudent(ginka);
        math1.addStudent(penka);
        math1.addStudent(ivan);
        math1.addStudent(stoqn);
        math1.addStudent(ivo);
        math1.addStudent(ivo);

        StudentGroup math2 = new StudentGroup("Math2");
        math2.addStudent(dragan);
        math2.addStudent(petkan);

        StudentGroup literature = new StudentGroup("Literature");
        literature.addStudent(puhi);
        literature.addStudent(pesho);

        StudentGroup history = new StudentGroup("History");
        history.addStudent(georgi);
        history.addStudent(dimitar);

        StudentGroup medical = new StudentGroup("Medical");
        medical.addStudent(aliRaza);
        medical.addStudent(ivanIvanov);
        medical.addStudent(georgiGeorgiev);
        medical.addStudent(perhunde);
        medical.addStudent(aliBaba);

        // System.out.println(math1.theBestStudent());
        System.out.println(medical.theBestStudent());
        math1.printStudentsInGroup();
        math2.printStudentsInGroup();
        literature.printStudentsInGroup();
        history.printStudentsInGroup();
        medical.printStudentsInGroup();
    }
}
