/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author slavi
 */
public class StudentGroup {

    private static final String ANSI_GREEN_FILL = "\u001B[42m";
    private static final String ANSI_YELLOW_FILL = "\u001B[43m";

    private String groupSubject;
    private Student[] students;
    private int freePlaces;

    public StudentGroup() {
        this.students = new Student[5];
        this.freePlaces = students.length;
    }

     StudentGroup(String subject) {
        this();
        if (subject != null && !subject.equals("")) {
            this.groupSubject = subject;
        }
    }

     void addStudent(Student s) {
        if (freePlaces <= 0) {
            System.err.println("There are no free places!");
            return;
        }
        if (!s.getSubject().equals(this.groupSubject)) {
            System.err.println("Enter the correct group!");
            return;
        }
        this.students[students.length - this.freePlaces] = s;
        this.freePlaces--;
    }

     void emptyGroup() {
        this.students = new Student[5];
        this.freePlaces = students.length;
    }

     String theBestStudent() {
        int maxGradeIndex = 0;

        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i] != null && this.students[i].getGrade() > this.students[maxGradeIndex].getGrade()) {
                maxGradeIndex = i;
            }
        }
        return this.students[maxGradeIndex].getName();
    }

     void printStudentsInGroup() {
        NumberFormat formatter = new DecimalFormat("#.##");

        String leftAlignFormat = "| %-2d | %-15s | %.3f |       %-8d | %-3d | %-8s  |   %-7b | %-11s |%n";
        System.out.format("+----+-----------------+-------+----------------+-----+-----------+-----------+-------------+%n");
        System.out.format(ANSI_GREEN_FILL + "| \u2116  " + "|      Name       | Grade | year in colage | age |   money   | is degree |   subject   |%n");
        System.out.format("+----+-----------------+-------+----------------+-----+-----------+-----------+-------------+%n");

        for (int i = 0; i < this.students.length; i++) {
            Student s = this.students[i];
            if (s == null) {
                break;
            }
            System.out.format(ANSI_YELLOW_FILL + leftAlignFormat, i + 1, s.getName(), s.getGrade(), s.getYearInCollage(), s.getAge(), formatter.format(s.getMoney()) + " \uD83D\uDCB0", s.isIsDegree(), s.getSubject());
        }
        System.out.format("+----+-----------------+-------+----------------+-----+-----------+-----------+-------------+%n %n");
    }
}
