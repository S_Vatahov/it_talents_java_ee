/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16_2;

/**
 *
 * @author slavi
 */
public class WorkerDemo {

    public static void main(String[] args) {
        Employee pesho = new Employee("Pesho");
        Employee puhi = new Employee("Puhi");

        pesho.setHoursLeft(8);
        puhi.setHoursLeft(8);
        pesho.setCurrentTask(new Task("hvarlq pari", 25));

        puhi.setCurrentTask(new Task("tancuva na pilon", 6));

        pesho.work();
        puhi.work();

       
    }
}
