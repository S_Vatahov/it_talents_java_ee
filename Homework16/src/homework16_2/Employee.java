/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework16_2;

/**
 *
 * @author slavi
 */
class Employee {

    private String name;
    private Task currentTask;
    private int hoursLeft;

    Employee(String name) {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            System.err.println("Incorrect name");
        }
    }

    void work() {
        if (currentTask == null) {
            System.err.println("The worker has no task");
            return;
        }
        if (currentTask.getWorkingHours() > this.getHoursLeft()) {
            currentTask.setWorkingHours(currentTask.getWorkingHours() - this.getHoursLeft());
            this.setHoursLeft(0);
        } else {
            this.setHoursLeft(this.getHoursLeft() - currentTask.getWorkingHours());
            currentTask.setWorkingHours(0);
        }
        showReport();
    }

    void showReport() {
        String leftAlginFormat = "| %-8s | %-17s | %-10d | %-15d |%n";
        System.out.format("+----------+-------------------+------------+-----------------+%n");
        System.out.format("|   Name   |     Task name     | Hours left | Task hours left |%n");
        System.out.format("+----------+-------------------+------------+-----------------+%n");

        System.out.format(leftAlginFormat, this.name, this.currentTask.getName(), this.hoursLeft, this.currentTask.getWorkingHours());

        System.out.format("+----------+-------------------+------------+-----------------+%n %n");

    }

    Task getCurrentTask() {
        return currentTask;

    }

    int getHoursLeft() {
        return hoursLeft;
    }

    void setCurrentTask(Task currentTask) {
        if (currentTask == null) {
            System.err.println("Incorrect task");
            return;
        }
        this.currentTask = currentTask;
    }

    void setHoursLeft(int hoursLeft) {
        if (hoursLeft > 100 || hoursLeft < 0) {
            System.err.println("Incorrect hours left");
            return;
        }
        this.hoursLeft = hoursLeft;
    }
}
