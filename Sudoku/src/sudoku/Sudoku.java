/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

import java.util.Scanner;

/**
 *
 * @author Слави
 *
 */
public class Sudoku {

    private static void printMatrix(int[][] matrix) {
        String horisontalHeavyLine = "\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u2501";
        String verticalHeavyLine = " \u2503 ";
        System.out.println("\u250f" + horisontalHeavyLine + "\u2513");
        for (int i = 0; i < matrix.length; i++) {

            if (i == 3 || i == 6) {
                System.out.println("\u2523" + horisontalHeavyLine + "\u252b");
                System.out.print("\u2503 ");
            } else {
                System.out.print("\u2503 ");
            }
            for (int j = 0; j < matrix.length; j++) {
                System.out.print((j == 2 || j == 5 || j == matrix.length - 1) ? matrix[i][j] + verticalHeavyLine : matrix[i][j] + "\u2502");
            }
            System.out.println();
        }
        System.out.println("\u2517" + horisontalHeavyLine + "\u251b");
    }

    private static int selectLevel() {
        Scanner input = new Scanner(System.in);
        int level;
        System.out.println("           SUDOKU\n");
        do {
            System.out.println("    Select game level\n");
            System.out.println("Level 1 Easy    \uD83D\uDE43 \nLevel 2 Medium  \uD83E\uDD14 \nLevel 3 Hard    \uD83D\uDE08 \nLevel 4 Evil    \uD83D\uDC7F");
            level = input.nextInt();
        } while (level <= 0 || level > 4);
        return level;
    }

    private static int[][] generateDigits(int[][] matrix, int level) {
        int digitsCount = 0;
        switch (level) {
            case 1:
                digitsCount = 35;
                break;
            case 2:
                digitsCount = 30;
                break;
            case 3:
                digitsCount = 25;
                break;
            case 4:
                digitsCount = 17;
                break;
        }
        while (digitsCount > 0) {
            int row = 0 + (int) (Math.random() * 8);
            int col = 0 + (int) (Math.random() * 8);
            if (matrix[row][col] == 0) {
                int numberOfAttempts = 0;
                while (true) {
                    int digit = 1 + (int) (Math.random() * 9);

                    if (isValidDigit(matrix, digit, row, col)) {
                        matrix[row][col] = digit;
                        digitsCount--;
                        break;
                    }
                    numberOfAttempts++;
                    if (numberOfAttempts > 500) {
                        break;
                    }
                }
            }

        }
        return matrix;
    }

    private static boolean isValidDigit(int[][] matrix, int digit, int row, int col) {

        for (int index = 0; index < matrix.length; index++) {
            if (matrix[row][index] == digit || matrix[index][col] == digit) {
                return false;
            }
        }
        int rowIndex = ((row / 3) * 3);
        int colIndex = ((col / 3) * 3);

        for (int i = rowIndex; i < rowIndex + 3; i++) {

            for (int j = colIndex; j < colIndex + 3; j++) {
                if (matrix[i][j] == digit) {
                    return false;
                }
            }
        }

        return true;
    }
    static int minBackTrackingCount;

    private static boolean findSolution(int[][] matrix) {

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (matrix[i][j] != 0) {
                    continue;
                }
                for (int digit = 1; digit <= 9; digit++) {

                    if (isValidDigit(matrix, digit, i, j)) {
                        matrix[i][j] = digit;
                        if (findSolution(matrix)) {
                            return true;
                        } else {
                            minBackTrackingCount++;
                            matrix[i][j] = 0;
                        }
                    }
                }
                return false;
            }
        }
        return true;
    }

    private static int[][] copyMatrix(int[][] matrix) {
        int[][] copyMatrix = new int[9][9];
        for (int i = 0; i < matrix.length; i++) {
            System.arraycopy(matrix[i], 0, copyMatrix[i], 0, matrix.length);
        }
        return copyMatrix;
    }

    private static boolean isEnd(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix1[j] == 0) {
                    return false;
                }
            }
        }
        System.out.println("            \u263A\u263A\u263A");
        return true;
    }

    private static boolean isZerro(int[][] matrix, int row, int col) {
        return matrix[row][col] == 0;
    }

    public static void main(String[] args) {

//        int[][] test = {
//            {0, 0, 4, 9, 0, 0, 6, 0, 0},
//            {0, 3, 0, 8, 0, 0, 0, 7, 0},
//            {7, 0, 0, 0, 0, 3, 0, 0, 8},
//            {0, 0, 9, 0, 0, 0, 0, 1, 4},
//            {0, 0, 0, 0, 2, 0, 0, 0, 0},
//            {8, 2, 0, 0, 0, 0, 3, 0, 0},
//            {9, 0, 0, 7, 0, 0, 0, 0, 2},
//            {0, 5, 0, 0, 0, 2, 0, 6, 0},
//            {0, 0, 6, 0, 0, 9, 7, 0, 0}
//        };
//        findSolution(test);
//        printMatrix(test);
        Scanner input = new Scanner(System.in);
        int[][] matrix = new int[9][9];
        int[][] userMatrix;
        int[][] helpMatrix;
        int level = selectLevel();
        int difficult = 0;
        switch (level) {
            case 1:
                difficult = 1000;
                break;
            case 2:
                difficult = 30000;
                break;
            case 3:
                difficult = 300000;
                break;
            case 4:
                difficult = 300000;
                break;
        }
        while (true) {
            matrix = generateDigits(matrix, level);
            userMatrix = copyMatrix(matrix);
            helpMatrix = copyMatrix(matrix);
            if (findSolution(matrix)) {
                if (minBackTrackingCount >= difficult) {
                    break;
                }
            }
            minBackTrackingCount = 0;
            matrix = new int[9][9];
        }

        printMatrix(userMatrix);
        while (!isEnd(userMatrix)) {
            int row, col, digit;
            do {
                System.out.print("Enter row [0...8] = ");
                row = input.nextInt();
            } while (row < 0 || row > 8);
            do {
                System.out.print("Enter col [0...8] = ");
                col = input.nextInt();
            } while (col < 0 || col > 8);
            do {
                System.out.print("Enter digit [1...9] Or 0 to print the decision !!! = ");
                digit = input.nextInt();
            } while (digit < 0 || col > 9);
            if (digit == 0) {
                System.out.println("            \u2639\u2639\u2639");
                printMatrix(matrix);
                break;
            }
            if (isZerro(helpMatrix, row, col)) {
                if (isValidDigit(userMatrix, digit, row, col)) {
                    userMatrix[row][col] = digit;
                    printMatrix(userMatrix);
                } else {
                    System.out.println("Invalid digit !!!");
                    printMatrix(userMatrix);
                }
            } else {
                System.out.println("You can not change the original matrix !!!");
                printMatrix(userMatrix);
            }
        }
    }
}
