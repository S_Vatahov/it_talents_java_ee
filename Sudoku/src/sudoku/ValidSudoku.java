/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudoku;

/**
 *
 * @author slavi
 */
public class ValidSudoku {

    public static boolean isValidDigit(int[][] matrix, int dig, int row, int col) {
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[row][i] == dig || matrix[i][col] == dig) {
                return false;
            }
        }
        int indexI = ((row / 3) * 3);
        int indexJ = ((col / 3) * 3);

        for (int i = indexI; i < indexI + 3; i++) {
            for (int j = indexJ; j < indexJ + 3; j++) {
                if (matrix[i][j] == dig) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isValidSudoku(int[][] matrix) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = 0;
                if (!isValidDigit(matrix, temp, i, j)) {
                    return false;
                }
                matrix[i][j] = temp;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[][] matrix = {
            {1, 4, 8, 6, 9, 5, 3, 2, 7},
            {2, 6, 7, 3, 4, 1, 8, 5, 9},
            {9, 3, 5, 2, 8, 7, 4, 6, 1},
            {8, 2, 6, 1, 3, 9, 5, 7, 4},
            {5, 1, 9, 4, 7, 2, 6, 3, 8},
            {4, 7, 3, 8, 5, 6, 9, 1, 2},
            {3, 5, 1, 9, 2, 8, 7, 4, 6},
            {7, 8, 2, 5, 6, 4, 1, 9, 3},
            {6, 9, 4, 7, 1, 3, 2, 8, 5}
        };
        System.out.println(isValidSudoku(matrix));
    }
}
