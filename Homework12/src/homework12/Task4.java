/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework12;

import java.util.Arrays;

/**
 *
 * @author slavi
 */
public class Task4 {

    private static int binarySearch(int[] arr, int x, int left, int right) {
        int mid = (left + right) / 2;
        if (left > right) {
            return mid;
        }
        if (x == arr[mid]) {
            return mid;
        }
        if (x < arr[mid]) {
            return binarySearch(arr, x, left, mid - 1);
        }
        if (x > arr[mid]) {
            return binarySearch(arr, x, mid + 1, right);
        }
        return -1;
    }

    private static int[] add(int[] arr, int element) {
        int index = binarySearch(arr, element, 0, arr.length - 1);
        if (arr[index] <= element) {
            index++;
        }
        int[] helpArr = new int[arr.length + 1];
        System.arraycopy(arr, 0, helpArr, 0, index);
        helpArr[index] = element;
        System.arraycopy(arr, index, helpArr, index + 1, arr.length - index);

        System.out.println(Arrays.toString(helpArr));
        return helpArr;
    }

    public static void main(String[] args) {
        int[] arr = {2, 3, 5, 7, 9, 11, 45};
        arr = add(arr, 1);
        arr = add(arr, 6);
        arr = add(arr, 46);
    }
}
