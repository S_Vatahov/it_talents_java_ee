/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework12;

/**
 *
 * @author slavi
 */
public class Task2 {

    private static int longestArea(int[] arr, int index, int elementCounter, int longestSeria) {
        if (index == arr.length - 1) {
            if (elementCounter > longestSeria) {
                return elementCounter;
            }
            return longestSeria;
        }
        if (arr[index] == arr[index + 1]) {
            elementCounter++;
        } else {
            if (elementCounter > longestSeria) {
                longestSeria = elementCounter;
            }
            elementCounter = 1;
        }
        return longestArea(arr, index + 1, elementCounter, longestSeria);
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 3, 7, 9, 9, 9, 9, 11, 11, 12, 14, 14, 14, 14};
        System.out.println(longestArea(arr, 0, 1, 0));
    }
}
