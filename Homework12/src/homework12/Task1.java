/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework12;

/**
 *
 * @author slavi
 */
public class Task1 {

    private static void bubbleSort(int[][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                boolean isSortet = true;

                for (int z = 0; z < arr.length - i; z++) {
                    for (int k = 0; k < arr[z].length; k++) {

                        if (k == arr[k].length - 1 && z < arr.length - 1) {
                            if (arr[z][k] > arr[z + 1][0]) {
                                int temp = arr[z][k];
                                arr[z][k] = arr[z + 1][0];
                                arr[z + 1][0] = temp;
                                isSortet = false;
                            }
                            break;
                        } else if (k == arr[k].length - 1 && z == arr.length - 1) {
                            break;
                        }
                        if (arr[z][k] > arr[z][k + 1]) {
                            int temp = arr[z][k];
                            arr[z][k] = arr[z][k + 1];
                            arr[z][k + 1] = temp;
                            isSortet = false;
                        }
                    }
                }
                if (isSortet) {
                    return;
                }
            }
        }
    }

    private static void printArr(int[][] arr) {

        for (int[] arr1 : arr) {
            for (int j = 0; j < arr1.length; j++) {
                System.out.print(arr1[j] + "|");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] arr = {
            {3, 4, 65, 87},
            {31, 42, 652, 87},
            {32, 43, 645, 847},
            {31, 47, 5, 7},};
        bubbleSort(arr);
        printArr(arr);
    }

}
