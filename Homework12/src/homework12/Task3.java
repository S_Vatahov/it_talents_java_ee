/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework12;

/**
 *
 * @author slavi
 */
public class Task3 {

    private static boolean isSortet(int[] arr, int index) {
        if (index == arr.length - 1) {
            return true;
        }
        if (arr[index] > arr[index + 1]) {
            return false;
        }
        return isSortet(arr, index + 1);
    }

    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 7, 9, 11, 12};
        System.out.println(isSortet(arr, 0));
    }
}
